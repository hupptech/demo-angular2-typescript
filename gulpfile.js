var gulp = require('gulp'),
	tsc = require('gulp-typescript'),
	tslint =  require('gulp-tslint'),
	config = require('./gulp.config')(),
	tsProject = tsc.createProject('tsConfig.json', { typescript: require('typescript') }),
	connect = require('gulp-connect')
	browserSync = require('browser-sync'),
	superstatic = require('superstatic')
	sass = require('gulp-sass');

var ENV = "";

gulp.task('compile-ts', function(){
	var sourceFiles = [ config.allTs ];
	var tsResult = gulp
					.src( sourceFiles )
					.pipe(tsc(tsProject));
	return tsResult.js
			.pipe( gulp.dest(config.tsOutputPath) );
});

gulp.task('moveHtml', function(){
	gulp.src( config.htmlFiles )
		.pipe(gulp.dest(config.tsOutputPath));
});

gulp.task('compile-sass', function(){
	gulp.src('./assets/styles/sass/**/*.scss')
	.pipe(sass().on('error', sass.logError) )
	.pipe(gulp.dest('./assets/styles/css/'));
});

 gulp.task('build', ['compile-ts', 'compile-sass'], function(){
 	gulp.start('compile-ts', 'compile-sass')
 });

 gulp.task('typings', function(){
 	var stream = gulp.src("./typings.json")
        .pipe(gulpTypings()); //will install all typingsfiles in pipeline. 
    return stream; 
 });

gulp.task('serve', ['compile-ts', 'compile-sass', 'moveHtml'], function(){
	gulp.watch([config.allTs], ['compile-ts']);
	gulp.watch([config.allStyles], ['compile-sass']);
	gulp.watch([config.htmlFiles], ['moveHtml']);
	
	var outputPath;
	if(process.env.NODE_ENV ="DEV")	outputPath='dev/**/*.js'
	else							outputPath='prod/**/*.js'
		
	gulp.run
	browserSync({
		port: 3010,
		files: [ 'index.html',outputPath , 'assets/templates/**/*.html', 'assets/styles/sass/**/*.scss' ],
		injectChanges: true,
		logFileChanges: false,
		logLevel: 'silent',
		notify: true,
		reloadDelay: 0,
		//serves index.html, set with baseDir './'
		server:{
			baseDir: ['./'],
			middleware: superstatic( {debug: false} )
		},
		//does it make reload faster??? does not seem like
		online: true
	});
});
gulp.task('default', [ 'serve']);

//===================TESING TASKS================================
// gulp.task('serve-test', ['compile-ts-unitTest'], function(){
// gulp.task('test', ['test-compileTS'], function(){
// 	// gulp.watch([config.unitTests], ['compile-ts-unitTest']);
// 	browserSync({
// 		port:3000,
// 		files: ["./src/test/index.html", './src/test/unit-test/*.js', ],
// 		injectChanges: true, 
// 		logFileChanges: false,
// 		logLevel: 'silent',
// 		notify: true,
// 		reloadDelay: 0,
// 		server: {
// 			baseDir: ['./'],
// 			index: './src/test/index.html',
// 			middleware: superstatic({debug: false})
// 		}
// 	});
// });

// gulp.task('test-compileTS', function(){
// 	var sourceFiles = [ './src/test/unit-testing/*.ts' ];

// 	var tsResults = gulp
// 					.src(sourceFiles)
// 					.pipe(tsc(tsProject));

// 	return tsResults.js
// 			.pipe( gulp.dest( './src/test/unit-testing/' ));
// });

//===================TESING TASKS================================
