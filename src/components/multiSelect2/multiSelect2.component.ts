/*import { Component, Input, Output, EventEmitter, NgModule } from '@angular/core';
import { Help } from '../body/help.component';
@Component({
	selector: 'multi-select2',
	template: `
	<div class="multiSelect-wrapper" [ngClass]="{open: isClicked}">
		<label>{{ title }}</label><help type="{{help}}" ></help>
		<div class="dropdown" (click)="isClicked = !isClicked">
			<span class="dropdown-text">
				  {{(dropdown_text)||'Select all that apply'}}
			</span>
		</div>
		<div class="dropdown-options">
		  <ul>
			<li *ngFor="let item of list">
			  <input type="checkbox"
				id="{{item.id}}"
				[(ngModel)]="item.isChecked"
				(ngModelChange)="handleChange()" />
			  <label attr.for="{{item.id}}">{{ item.label }}</label>
			</li>
		  </ul>
    </div>
  </div>
  `,
  	directives: [
		Help
	]
})
export class MultiSelect2 {
	isClicked = false;
  @Input() list;
  @Input() title;
  @Input() help;
 // @Input() dropdown-text;
  // @Output indicates that this component will emit an event to any listeners
  @Output() 
  onChange = new EventEmitter<String>();
  dropdown_text: string ="";
  handleChange() {
    this.onChange.emit(this.getAllChecked(this.list));
    this.dropdown_text = this.changeDropDownText(this.list);
  }
  
  // a utility library like lodash would work wonders instead of writing everything ourselves
  private getAllChecked(list) {
    var selectedList = [];
    list.forEach(function(i){
      if(i.isChecked) {
        selectedList.push(i.id);
      }
    });
    return selectedList;
  }
  
  private changeDropDownText(list){
    var dropdownText = "Select all that apply",
    numberSelected=0;
    list.forEach(function(i){
      if(i.isChecked) {
        numberSelected++;
        dropdownText=i.label;
      }
    });
    if (numberSelected > 1){
        dropdownText="Selections ["+numberSelected+"]";
    };
    return dropdownText;
  } 
  
}*/