import {Component, ElementRef, Input, Output} from '@angular/core';
import { ModalService } from '../../services/modalService.service';
import {Router} from '@angular/router';
import {CurrentForm} from '../../services/currentForm.service';

@Component({
	host: {
		'(document:click)': 'showSubmenu($event)'
	},
	selector: 'edit',
	templateUrl: '../../assets/templates/body/editIcon.template.html'
})export class EditIcon{
	show_submenu:boolean = true;
	modalContent = {
		title: "Just Checking"
		,body: "Do you want to permanently remove this entry?"
		,buttons:{
			ok: "Remove this entry"
			,cancel: "Cancel"
		}
	};

	@Input() thisCard;
	
	constructor(
		private elem:ElementRef
		,private modal:ModalService
		,private router:Router
		,private currentForm:CurrentForm
	){}

	ngOnInit(){ /* console.log( this.thisCard );*/ }

	showSubmenu( event ){
		var clickedComponent = event.target;
       	var inside = false;
       	do {
			if (clickedComponent === this.elem.nativeElement) {
				inside = true;
			}
			/*walk up the DOM tree*/
			clickedComponent = clickedComponent.parentNode;
		} while (clickedComponent);

		this.show_submenu = (inside)?false:true;
		
	}

	removeCard(cardId){
		console.log("removing card: "+cardId);
		this.modal.setModalContent( this.modalContent );
		// this.modal.setModalContent( this.modalContent );
		this.currentForm.setModalDestination('delete');
		this.currentForm.setIdToDelete(cardId);
		this.modal.open();
		//fire modal...
			// Title: Just Checking
			// Body: Do you want to permanently remove this entry?
			//Button:
				// Remove this Entry
				// Cancel
		// get card Id so I can be removed
	}
	editCard(myid){
		//there must be a way to reference a child route w/o explicitly reference its parent
		console.log( "experience id: "+myid );
		this.router.navigate(['/Experience/Edit-Experience/'+myid]);
	}
}