import { Component, Input } from '@angular/core';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { AddRemoveClass } from '../../services/addRemoveClass.service';
import { EraseContent } from './eraseContent.component';
import { FireValidation } from '../../services/fireValidation.service';

@Component({
	selector: 'phonenumber-masked',
	// directives: [EraseContent],
	template: `
		<div [formGroup]="phoneCtrl">
			<input 
				type="tel"
				maxlength="12"
				name="phone"
				formControlName="phone"
				(keypress)="maskIt($event)"
				(click)="showEraseComp($event)"
				(focusout)="focusOut($event)"
			/>
			<erase-content (click)="eraseContent('phone')"></erase-content>
			<control-messages 
				class="form-error" 
				[control]="phoneCtrl.controls.phone"
			></control-messages>
		</div>
	`
})export class PhoneNumberMasked{
	@Input('phoneControl') public phoneCtrl:FormGroup;

	constructor(
		private ARC:AddRemoveClass
		,private fire:FireValidation
	){}
	
	maskIt( $event ){
		//filters out any typped letters
		if($event.keyCode >= 48 && $event.keyCode <= 57 && $event.target.value.length <= 11){
			//when input reaches index==3 add a dash 
			if($event.target.value.length == 3){
				$event.target.value = $event.target.value +"-";
			//when input reaches index==7 add a dash 
			}else if($event.target.value.length == 7){
				$event.target.value = $event.target.value +"-";	
			}
		//any other input besides number will return false (nothing)
		}else 	return false;
	}

	showEraseComp(event){
		this.ARC.RemoveClass(event.srcElement.nextElementSibling.firstChild, "hide-erase-comp");
		this.ARC.AddClass(event.srcElement.nextElementSibling.firstChild, "show-erase-comp");
	}

	focusOut(event){
		let that = this;
		// wait 1 ms so event does not disappear
		setTimeout(function(){
			that.ARC.RemoveClass(event.srcElement.nextElementSibling.firstChild, "show-erase-comp");
			that.ARC.AddClass(event.srcElement.nextElementSibling.firstChild, "hide-erase-comp");	
		}, 100);
		// console.log(event.target.name);
		// this.fire.fireIt(event.target.name );
	}

	eraseContent(id){
		this.phoneCtrl.controls[id].setValue('');
	}
}