import { Component, HostListener, ElementRef, Input, Output, EventEmitter } from '@angular/core';
import { NgSwitch, NgSwitchCase } from '@angular/common';
import { FOP } from '../../constants/fop.constant';
import { DISCIPLINES } from '../../constants/disciplines.constant';
import { MultiSelectFiller } from '../../services/multiSelectFiller.service';
// import { Subscription } from 'rxjs/Subscription';
import { AddRemoveClass } from '../../services/addRemoveClass.service';
import { User} from '../../services/httpServices/user.service';
import { CurrentInfo } from '../../services/currentInfo.service';
// import { CurrentForm } from '../../services/currentForm.service';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';

interface ResponseObject{
	
		professionalTitle?:string;
		firstName?:string;
		lastName?:string;
		location?:string;
		npiNumber?:number;
		phoneNumber?:number;
		personalStatement?:string;
		languageProficiencies?:Array<string>;
		profileFieldOfPractices?:Array<string>;
		profileDisciplines?:Array<string>;
		profileSpecialties?:Array<string>;
		profileLicenses?: Array<string>;
		profileBhwPrograms?: string;
		email?: string;
	
}

@Component({
	selector: "multi-select",
	// styleUrls: ["../../../assets/styles/css/multiSelect.css"],
	styles: [
		`
			ul{
				max-height: 200px;
				overflow-y:auto;
				padding: 0px;
				border: 1px solid #000;
				background: lightyellow;
			}
			li{
				list-style-type: none;
				cursor: pointer;
			}
			li:hover{
				background: lightgrey;
				color: #fff;
			}
			.show{
				display:block;
			}
			.hide{
				display:none;
			}
		`
	],
	template: `
		<div  class="multiSelect-wrapper" [formGroup]="control" >

			<!------------- DROPDOWN TEXT ------------->
			<div class="dropdown">
				<span class="dropdown-text">
					<span [hidden]="showText" id={{type}} >
						{{(dropdown_text)||'Select all that apply'}}
					</span>
				</span>
			</div>
			<div [ngSwitch]="type" [hidden]="showThis" >
				<!------------- FIELD OF PRACTICE------------->
				<ul  *ngSwitchCase="'fop'"  >
					<li *ngFor="let fop of fopArr" (click)="checkFOP(fop)">
						<input 
							type="checkbox" 
							formControlName="profileFieldOfPractices"
							[checked] = "fop.checked"
						/>
						<label>&nbsp;{{fop.name}}</label>
					</li>
				</ul>
				<!------------- DISCIPLINES ------------->
				<ul *ngSwitchCase="'discipline'" >
					<li *ngFor="let disc of DISC" (click)="checkDISC(disc)">
						<input 
							type="checkbox" 
							formControlName="profileDisciplines"
							[checked] = "disc.checked"
						/>
						<label>&nbsp;{{disc.name}} - <span style="color:red;">{{disc.parent}}</span></label>
					</li>
				</ul>
				<!------------- SPECIALTIES ------------->
				<ul *ngSwitchCase="'specialty'" >
					<li *ngFor="let spec of SPEC" (click)="checkSPEC(spec)">
						<input 
							type="checkbox" 
							formControlName="profileSpecialties"
							[checked] = "spec.checked"
						/>
						<label>&nbsp;{{spec.name}}-<span style="color:red;">{{spec.parent}}</span></label>
					</li>
				</ul>
			</div>
		</div>
	`
	,host: {
		'(document:click)': 'toggleDropdown($event)'
	}
})export class MultiSelect{
	@Input('group') public control:FormGroup;
	@Input() type;

	@Output() fopArr = [];
	@Output() discArr = [];
	@Output() specArr = [];
	
	FOPcounter:number = 0
	// @Output() SPECcounterEE = new EventEmitter();
	DISCcounter:number = 0;
	// @Output() DISCcounterEE = new EventEmitter();
	SPECcounter:number = 0;

	@Output() dropdown_text:string="";
	@Output() showThis :boolean = true;
	@Output() FireDropdown = new EventEmitter(true);
	

	constructor(
		private multi: MultiSelectFiller
		,private currentInfo: CurrentInfo
		,private eref:ElementRef
		,private ARC:AddRemoveClass
		,private user:User
	){
		this.FireDropdown.subscribe((isShow)=>{
			this.showThis = isShow;
		});
	}
	elem;
	//async checks if array has change and returns most up to date values to ngFor
	get DISC(){ return this.multi.getDiscArr(); }
	get SPEC(){ return this.multi.getSpecArr(); }
	
	ngOnInit(){
		this.user.getUser().subscribe((obj)=>{
			//gets current info
			//gets field of practices
			let fop = obj.profileFieldOfPractices; //"DENTAL", "MENTAL_HEALTH"
			let disc = obj.profileDisciplines; //"DENTIST", "LICENSED_CLINICAL_SOCIAL_WORKER", "NURSE_PRACTITIONER"
			let spec = obj.profileSpecialties; //"PEDIATRICS", "NONE", "ADULT"
			
			//For testing...it clears any content that the arrays might have
			/*
				fop = [];
				disc = [];
				spec = [];
			*/

			if(this.type == 'fop' ){
				this.setFieldOfPractice(fop);
				//FOP will always display whether there is data or not
				if(fop.length == 0)
					this.dropdown_text = "Select all that apply";
				if(fop.length == 1 ){
					this.dropdown_text = this.toTitleCase(fop[0].toLowerCase() ); 
				}else if( fop.length >1){
					this.dropdown_text = "Selected["+fop.length+"]";
				}
			}
			
			if(this.type == 'discipline'){
				this.setDisciplines(fop, disc);
				//if there is no data from server...hide dropdown
				if(disc.length == 0){
					this.ARC.RemoveClass(this.eref.nativeElement.parentElement, 'show');
					this.ARC.AddClass(this.eref.nativeElement.parentElement, 'hide');
					this.dropdown_text = "Select all that apply";
					
				}else if(disc.length == 1 ){
					this.dropdown_text = this.toTitleCase(disc[0].toLowerCase() ); 
					
				}else if( disc.length >1){
					this.dropdown_text = "Selected["+disc.length+"]";
				}
					
			} 	
			if(this.type == 'specialty'){
				this.setSpecialties(fop, disc, spec);
				
				if(spec.length == 0){
					this.ARC.RemoveClass(this.eref.nativeElement.parentElement, 'show');
					this.ARC.AddClass(this.eref.nativeElement.parentElement, 'hide');
					this.dropdown_text = "Select all that apply";
				}else if(spec.length == 1 ){
					this.dropdown_text = this.toTitleCase(spec[0].toLowerCase() ); 
				}else if( spec.length >1){
					this.dropdown_text = "Selected["+spec.length+"]";
				}
			}
		});
		
	}
	toggleMultiSelect(){}

	setFieldOfPractice(obj){
		this.FireDropdown.emit(true);
		this.populateAndCheck( FOP, obj, this.fopArr );
	}

	setSpecialties(fop, disc , spec){
		fop.map((myfop)=>{
			this.SPECcounter ++;
			DISCIPLINES[ myfop.toLowerCase() ].map((v)=>{
				disc.map((mydisc)=>{
					if( v.name.split(" ").join("_").toLowerCase() == mydisc.toLowerCase() ){
						v.children.map((myspec)=>{
							this.specArr.push(myspec);
							spec.map((s)=>{
								if(myspec.name.split(" ").join("_").toLowerCase() == s.toLowerCase() ){
									myspec.checked = true;
								}
							});
						});
					}
				});
			});
		});
		this.multi.setSpecArr(this.specArr);
	}

	setDisciplines(fop, disc){
		fop.map((myfop)=>{
			DISCIPLINES[myfop.toLowerCase()].map((v)=>{
				this.discArr.push(v);
				disc.map((mydisc)=>{
					if(mydisc.toLowerCase() == v.name.split(" ").join("_").toLowerCase()){
						this.DISCcounter++;
						v.checked = true;
					}
				});
			});
		});
		// console.log(this.DISCcounter);
		//sets arr to a service so it can accesible anytime during the application
		this.multi.setDiscArr(this.discArr);
	}
	checkFOP(myfop){
		let discArr = [];
		let specArr = [];
		let fopArr = [];

		//if option is checked, then remove data from next dropdown and uncheck property
		
		if(myfop.checked){
			myfop.checked = false;
			this.FOPcounter--; //if fop is uncheck, updates counter
			this.multi.getDiscArr().map((mydisc)=>{
				
				if(mydisc.parent != myfop.name.toLowerCase().split(" ").join("_") ){
					
					specArr = [];
					if(mydisc.checked){
						mydisc.children.map((disc)=>{
							specArr.push(disc);
							this.multi.setSpecArr(specArr);
						})
					}
					discArr.push( mydisc );
				}
			});
		}else{
			myfop.checked = true;
			//sets FOP array with clicked options
			fopArr = this.multi.getFopArr();
			fopArr.push(myfop);
			//===============================================
			this.FOPcounter++; // updates counter
			DISCIPLINES[myfop.name.toLowerCase().split(" ").join("_")].map((mydisc)=>{
				//get array with values already there
				discArr = this.multi.getDiscArr();
				//and then push new values to already existing array
				specArr = [];
				if( mydisc.checked ){
					mydisc.children.map((disc)=>{
						specArr.push(disc);
						this.multi.setSpecArr(specArr);
					})
				}
				discArr.push(mydisc);
			});
		}
		this.multi.setFopArr( fopArr );

		//updates dropdown text
		let nextElem = this.eref.nativeElement.parentElement.nextElementSibling;
		if(this.FOPcounter == 0){
			this.dropdown_text = "Select all that apply";
			this.ARC.RemoveClass(nextElem, 'show');
			this.ARC.AddClass(nextElem, 'hide');
			this.multi.setSpecArr( [] );
	

		}else if(this.FOPcounter == 1){

			this.ARC.RemoveClass(nextElem, 'hide');
			this.ARC.AddClass(nextElem, 'show');
			let fop = this.fopArr.filter((fop)=>{
				return fop.checked
			});
			this.dropdown_text = fop[0].name;
	
		}else if( this.FOPcounter > 1){
			this.dropdown_text = "Selected["+this.FOPcounter+"]";
		}
		/*
			//this only needs to run if there is at least one value check in DISC
			//updates specialties on FOP selection
		*/
		let isCheck = this.multi.getDiscArr().find(
			mydisc=> { return (mydisc.checked) }
		);
		//needs to also check if discipline is showing....
		//sets specArr to service
		this.multi.setSpecArr(specArr);
		//sets discArr to service
		this.multi.setDiscArr(discArr);

		if( isCheck!=undefined  ){
			this.multi.getDiscArr().map((myfop)=>{
				if(myfop.checked){
					DISCIPLINES[myfop.parent.toLowerCase().split(" ").join("_")].map((fop)=>{
						if(fop.checked){
							fop.children.map((ch)=>{
								specArr.push(ch);
							});
							this.multi.setSpecArr(specArr);
						}else{
							let check = this.multi.getDiscArr().find(
								mydisc=> { return (mydisc.checked) }
							);
							if(!check){
								specArr = [];
								this.multi.setSpecArr(specArr);
							}
							
						}
					});
				}
			});	
		}
		//Get current count of check items after FOP has been changed
		//======================================================
		// type = fop || discipline || specialty
		// {{type+'text'}}
		let disccounter:number = 0;
		let speccounter:number = 0;
		this.multi.getDiscArr().map((mydisc)=>{
			if(mydisc.checked) disccounter++;
		});
		// console.log("DISC COUNTER "+disccounter);
		this.multi.getSpecArr().map((myspec)=>{
			if(myspec.checked)	speccounter++
		});
		// console.log("SPEC COUNTER "+speccounter)
		// console.log( document.getElementById('discipline') );
		// console.log( document.getElementById('specialty') );

		// console.log( document.getElementById('discipline').innerText );
		document.getElementById('discipline').innerText = "Selected["+disccounter+"]";
		document.getElementById('specialty').innerText = "Selected["+speccounter+"]";
		//=====================================================
	}
	checkDISC(mydisc){
		let specArr = [];
		//if option is checked then unchecked value and remove data from specArr
		
		if(mydisc.checked){
			mydisc.checked = false;
			this.DISCcounter--;
			this.multi.getSpecArr().map((myspec)=>{
				// console.log(mydisc)
				if(myspec.parent.toLowerCase() != mydisc.name.toLowerCase().split(" ").join("_") )
					specArr.push(myspec);
			});
		}else{
			mydisc.checked = true;
			this.DISCcounter++;
			DISCIPLINES[mydisc.parent.toLowerCase().split(" ").join("_")].map((child)=>{
				specArr = this.multi.getSpecArr();
				
				if( mydisc.name.toLowerCase().split(" ").join("_") == child.name.toLowerCase().split(" ").join("_") ){
					mydisc.children.map((ch)=>{
						specArr.push(ch);
					})
				}
			});
		}
		//updates dropdown text
		let nextElem = this.eref.nativeElement.parentElement.nextElementSibling;
		if(this.DISCcounter == 0){
			this.dropdown_text = "Select all that apply";
			this.ARC.RemoveClass(nextElem, 'show');
			this.ARC.AddClass(nextElem, 'hide');
		}else if(this.DISCcounter == 1){
			// console.log(this.multi.getDiscArr() );
			this.ARC.RemoveClass(nextElem, 'hide');
			this.ARC.AddClass(nextElem, 'show');
			let fop = this.multi.getDiscArr().map((fop)=>{
				if(fop.checked)	return fop;
			});
			fop = fop.filter((val)=>{
				return (val!=undefined)
			})
			// console.log(fop);
			this.dropdown_text = fop[0].name;
		}else if( this.DISCcounter > 1){
			this.dropdown_text = "Selected["+this.DISCcounter+"]";
		}
		// console.log( newArr );
		this.multi.setSpecArr( specArr );

		//==================================================================
		let speccounter:number = 0;
		this.multi.getSpecArr().map((myspec)=>{
			if(myspec.checked)	speccounter++
		});
		console.log("SPEC COUNTER "+speccounter)
		
		document.getElementById('specialty').innerText = "Selected["+speccounter+"]";
		//===================================================================
	}
	checkSPEC(myspec){
		if(myspec.checked){
			myspec.checked = false;
			this.SPECcounter--;
		}else{
			myspec.checked = true;
			this.SPECcounter++;
		}

		if(this.SPECcounter == 0){
			this.dropdown_text = "Select all that apply";
		}else if(this.SPECcounter == 1){
			let specArr = this.multi.getSpecArr();
			// console.log( specArr );
			let spec = specArr.map((spec)=>{
				if(spec.checked)	return spec
			});
			spec = spec.filter((val)=>{
				return (val!=undefined)
			})
			
			this.dropdown_text = spec[0].name;
		}else if( this.SPECcounter > 1){
			this.dropdown_text = "Selected["+this.SPECcounter+"]";
		}
	}

	//============= HELPER FUNCTIONS ======================
	toTitleCase(str){
		return str.replace(/\w\S*/g, function(txt){return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();});
	}
	populateAndCheck(CONST, res, typeArr){
		let arr = [];
		CONST.map((fop)=>{
			typeArr.push(fop);
			res.map((val)=>{
				if(val.toLowerCase() == fop.name.split(" ").join("_").toLowerCase()){
					arr.push(fop);
					// this.multi.setFopArr(fop);
					fop.checked = true;
					this.FOPcounter++; //updates counter of how many fop are checked
				};
			});
		});
		this.multi.setFopArr(arr);
	}
	toggleDropdown(event){
		var clickedComponent = event.target;
		var inside = false;
		/*walks up the DOM tree, same logic as linkedlistS*/
		do {
			if (clickedComponent === this.eref.nativeElement) inside = true;

			clickedComponent = clickedComponent.parentNode;
		} while (clickedComponent);
		// console.log(this.eref.nativeElement.childNodes[1] );

		//got no idea why eref is getting undefined...this will make it work, fix later
		if(this.eref.nativeElement.childNodes[1]!=undefined)
			this.elem = this.eref.nativeElement.childNodes[1];
		// console.log(this.elem);
		if( inside ){
			// console.log("inside");
			//sets all this toggling classes to be a VARIABLE and binded
			this.ARC.RemoveClass( this.elem, "deactive-container" );
			this.ARC.AddClass( this.elem, "active-container");
			this.FireDropdown.emit( false );
		}else{
			// console.log("outside");
			this.ARC.RemoveClass( this.elem, "active-container");
			this.ARC.AddClass( this.elem , "deactive-container" );
			this.FireDropdown.emit( true );
		}
	}
}