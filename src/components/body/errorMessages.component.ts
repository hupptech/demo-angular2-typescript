/*
	TODO
	+ For some reason errorMessage gets reset everytime .next is perform from service
	I tried to reinstantiate the service so each component will have a clean service variables but it does not work
	I have implemented a switch statement as a work around to set correct error message for each field.
	PLEASE!!! think of a better way to do this...seems too redudant
*/

import {Component, Input } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { ErrorService } from '../../services/error.service';
import {Subscription } from 'rxjs/Subscription';
import {ValidationService} from '../../services/validationService.service';

@Component({
	selector:"error-message",
	// providers: [ErrorService],
	template:'<div [hidden]="(errorMessage==null)" >{{_errorMessage}}</div>'
	//so each field would get a different error, no sharing same service variables
	
})export class ErrorMessage{
	subscription:Subscription;
	@Input() control:FormControl;
	_errorMessage:string = null;
	
	constructor(private error:ErrorService){
		this.subscription = error.thisError.subscribe((v)=>{
			if(v!="" && this.control!=undefined)
				this._errorMessage = this.setErrorMessage()
		});
	}

	get errorMessage(){
		// console.log( this.control );
		if( this.control.errors ){
			for(let fld in this.control.errors){
				if(this.control.touched){
					this._errorMessage = ValidationService.getValidatorsErrorMessage(fld);
				}
			}
		}else{
			this._errorMessage = null;
		}
		return this._errorMessage;
	}
	set errorMessage(v){ 
		this._errorMessage = v;
	}
	setErrorMessage(){
		for(let fld in this.control.errors){
			switch(fld){
				case 'invalidFirstname':return "Invalid Firstname";
				case 'invalidLastname':	return "Invalid Lastname";
				case 'invalidLanguage':	return "Invalid Language";
				
			}
		}
	}
}