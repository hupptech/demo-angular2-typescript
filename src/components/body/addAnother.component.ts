import { Component,ViewChild, Input, Output,ViewContainerRef } from '@angular/core';
// import { Language } from './container.component';
// import { SetTemplate } from  '../../services/setTemplate.service';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { ValidationService } from '../../services/validationService.service';
import { GetLanguage } from '../../services/getLanguage.service';
import { GetStates } from '../../services/getLanguage.service';
// import { Subscription } from 'rxjs/Subscription';
import { ErrorMessage } from './errorMessages.component';
import { EraseContent } from './eraseContent.component';
import { AddRemoveClass } from '../../services/addRemoveClass.service';

@Component({
	selector: 'add-another'
	,template: `
		<div *ngIf="(type=='language')">
			<div class="container-wrapper language-block" [formGroup]="thisForm">
				<input
					[attr.list]="languageId"
					name="language"
					type="text"
					maxlength= "50"
					(click)="showEraseComp($event)"
					(focusout)="focusOut($event)"
					(keyup)="callService($event, 'language')"
					value="Select One"
					formControlName = "language"
					[class.error]="!thisForm.controls.language.valid && (thisForm.controls.language.touched || submitAttempt)"
				/>
				<erase-content (click)="eraseContent('language')"></erase-content>
				<error-message
					[control]="thisForm.controls.language"
					class="form-error"
				></error-message>
				<datalist [id]="languageId">
					<option *ngFor="let val of langArr" value="{{val}}">{{val}}</option>
				</datalist>
				<div class="select-wrapper" >
					<select class="proficiencies" formControlName="languageProficiency">
						<option value="" selected>Select One</option>
						<option value="ELEMENTARY_PROFICIENCY">Elementary Proficiency</option>
						<option value="LIMITED_PROFICIENCY">Limited Working Proficiency</option>
						<option value="PROFESSIONAL_PROFICIENCY" >Professional Working Proficiency</option>
						<option valuE="FULL_PROFICIENCY">Full Professional Proficiency</option>
						<option value="NATIVE_OR_BILINGUAL_PROFICIENCY">Native or Bilingual Proficiency</option>
					</select>
				</div>
				<error-message
					[control]="thisForm.controls.languageProficiency"
					class="form-error"
				></error-message>
			</div>
		</div>
		<div *ngIf="(type=='state')" >
			<div [formGroup]="thisForm" >
				<input
					[attr.list]="stateId"
					maxlength="50"
					type="text"
					name="state"
					formControlName="stateLicense"
					(keyup)="callService($event, 'state')"
					placeholder="Select"

				/>
				<error-message
					[control]="thisForm.controls.stateLicense"
					class="form-error"
				></error-message>
				<datalist [id]="stateId">
					<option *ngFor="let state of statesArr" value="{{state}}">{{state}}</option>
				</datalist>
			</div>
		</div>
	`
	
})export class AddAnother{
	@Input() type;
	@Output() typeText:string="Default";
	@Input('group') public thisForm: FormGroup;
	isShow:boolean = true;
	
	langArr:Array<string> = [];
	statesArr:Array<string> = [];
	languageId:number = 0;
	stateId:number = 0;
	
	constructor(
		private view:ViewContainerRef
		,private lang:GetLanguage
		,private state:GetStates
		,private ARC:AddRemoveClass
	){}

	showEraseComp(event){
		// console.log("here;");
		// console.log(event);
		this.ARC.RemoveClass(event.srcElement.nextElementSibling.firstChild, "hide-erase-comp");
		this.ARC.AddClass(event.srcElement.nextElementSibling.firstChild, "show-erase-comp");
	}
	focusOut(event){
		// console.log("focusout");
		let that = this;
		// wait 1 ms so event does not disappear
		setTimeout(function(){
			that.ARC.RemoveClass(event.srcElement.nextElementSibling.firstChild, "show-erase-comp");
			that.ARC.AddClass(event.srcElement.nextElementSibling.firstChild, "hide-erase-comp");	
		}, 100);
		// console.log(event.target.name);
		// this.fire.fireIt(event.target.name );
	}

	ngOnInit(){
		// console.log(this.thisForm);
		this.languageId = Math.random();
		this.stateId = Math.random();
	}

	callService(event, which){
		if(which=="language")
			this.langArr = this.lang.getLanguage( event.srcElement.value.length, event.target.value );
		else if(which=="state")
			this.statesArr = this.state.getStates( event.srcElement.value.length, event.target.value )
	}
	eraseContent(id){		
		this.thisForm.controls[id].setValue('')
	}
}