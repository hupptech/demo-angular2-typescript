import { Component } from '@angular/core';
import { Location } from '@angular/common';
import { Http, Headers } from '@angular/http';
import { CurrentForm } from '../../services/currentForm.service';
import { User } from '../../services/httpServices/user.service'
import { ExperienceService } from '../../services/httpServices/experience.service'

@Component({
	selector: 'this-save',
	templateUrl: '../../assets/templates/body/thisSave.template.html'
})export class ThisSave{
	
	constructor(
		private http:Http
		,private currentForm:CurrentForm
		,private user:User
		,private location:Location
		,private experience:ExperienceService
	){}

	saveThis(){
		
		if(!this.currentForm.getIsSave()){
			for(let field in this.currentForm.controls){
				if(!this.currentForm.controls[field].pristine){
					this.currentForm.setDirty(true);
				}
			}
		}
		//gets what location the user is currently on
		let location = this.getLocation( this.location.path() );

		if(this.currentForm.status == "VALID" && this.currentForm.getControls() != null ){
			if( this.currentForm.getDirty() ){

				switch(location){
					/*TODO*/
					/*
						+ we need to have a way to know what endpoint is gonna be called
						+ POST|PUT|GET|DELETE
					*/
					case 'Experience':
						//will edit an experience
						console.log( this.currentForm );
						if(this.location.path().indexOf('Edit')>0)
							this.experience.updateExperience( this.currentForm.getControls(), this.location.path().split("/")[3] );
						//will create experience
						else if(this.location.path().indexOf('Create')>0)
							this.experience.createExperience(this.currentForm.getControls() );
					break;
					case 'PersonalInformation':
						this.user.setUserObj( this.currentForm.controls );
						this.user.postUser();
					break;
				}
				//sets form state to saved...so other events will know that form has been saved
				this.currentForm.setIsSave(true);
				//sets form to pristine
				this.currentForm.setDirty(false);
			}else 	console.info("Nothing to save")
			
		}else{
			console.info("This form is INVALID to save data or form controles has not been set")
		}
	}

	getLocation(path){
		if(path.indexOf("Experience")>0){
			return 'Experience';
		}else if(path.indexOf('Personal_Information')>0){
			return "PersonalInformation"
		}
	}
}