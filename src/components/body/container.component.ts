import { ViewChild, Component, Input, Output } from '@angular/core';
import { SetTemplate } from '../../services/setTemplate.service';
import { Subscription } from 'rxjs/Subscription';
import { GetLanguage } from '../../services/getLanguage.service';
import { GetStates } from '../../services/getLanguage.service';
import { FormGroup } from '@angular/forms';
import { ValidationService } from '../../services/validationService.service';
import { FireValidation } from '../../services/fireValidation.service';

@Component({
	selector: 'container',
	templateUrl: '../../assets/templates/body/container.template.html'
})
export class Language{
	// @Input() Group;
	@Input() type;
	@Output() languageType:boolean;
	@Output() stateType:boolean;
	@Input('Group') public languageForm: FormGroup;

	subscription:Subscription;
	langArr:Array<string> = [];
	statesArr:Array<string> = [];
	languageId:number = 0;
	stateId:number = 0;

	constructor( 
		private setTemplate:SetTemplate, 
		private lang:GetLanguage,
		private states:GetStates,
		private fire: FireValidation
	){
		console.log(this.languageForm);
	}

	ngOnInit(){
		this.languageId = Math.random();
		this.stateId = Math.random();

		this.subscription = this.setTemplate.languageShow.subscribe(t=>{	
			this.languageType = t.language;
			this.stateType = t.state;
		});

		this.states.thisResp.subscribe(function(val){});

		if( this.type == "language" ){
			this.languageType = true;
			this.stateType = false;
		}else if(this.type == "stateLic"){
			this.stateType = true;
			this.languageType = false;
		}
	}
	
	searchStates(event){
		this.statesArr = this.states.getStates( event.srcElement.value.length, event.target.value );
	}
	
}