import { Component, Input, Output, EventEmitter, Inject, ElementRef } from '@angular/core';
import { Router } from '@angular/router';
import { AddRemoveClass } from '../../services/addRemoveClass.service';
import { HOME_OPTIONS } from '../../constants/homeOptions.constant';
import { TitleService } from '../../services/thisTitle.service';
import { ShowDivs } from '../../services/showDivs.service';
import { Location } from '@angular/common';
import { ModalService } from '../../services/modalService.service';
import { CompleteSections } from '../../services/completeSections.service';
import {CurrentForm} from '../../services/currentForm.service';
import {User} from '../../services/httpServices/user.service';

@Component({
	selector: 'my-body',
	templateUrl: '../../assets/templates/body/body.template.html',
	providers: [ AddRemoveClass ]
	,styles: [
		`
			.active-view{
				background: lightgrey;
				display:inline-block;
				vertical-align: middle;
				padding: 0px;
			}
			.passive-view{
				display:none;
			}
		`
	]
})export class Body{
	home_options : Array<string>;
	isHome:boolean;
	
	constructor( 
		private ARC:AddRemoveClass
		,private title:TitleService
		,private show:ShowDivs
		,private location:Location
		,private modal:ModalService
		,private completeSections: CompleteSections
		,private currentForm:CurrentForm
		,private router:Router
		,private user:User
		,private eref:ElementRef
	){
		this.ARC = ARC;
		// return string so it is readable, not concatanated with "_"
		this.home_options = HOME_OPTIONS.map( i=>{
			return i.split(" ").join("_");
		});
		
		this.show.isShow.subscribe(val=>{
			this.isHome = val;
		});
		
	}

	ngAfterViewInit(){
		//make sure it is not home being passed
		if(this.location.path()!="")
			this.toggleActive( null, this.location.path() );
	}
	openModal(){
		this.modal.open();
	}

	closeModal(){}

	takeMeHome(){
		if(!this.currentForm.getIsSave()){
			for(let field in this.currentForm.controls){
				// console.log( this.currentForm.controls[field].pristine );
				if(!this.currentForm.controls[field].pristine){
					
					this.currentForm.setDirty(true);
				}
			}
		}
		let formDirty:boolean = this.currentForm.getDirty();
		if( formDirty ){
			//valid form
			if(this.currentForm.status != "INVALID"){
				this.currentForm.setModalDestination("home");
				this.modal.open();
			}
		//form is clean and no changes have been made
		}else{
			this.router.navigate(['']);
		}

	}
	fillRouterOutlet(e, flag){
		/*console.log("===============")
		console.log( this.currentForm )*/
		//gets order-position of the option clicked 
		let pos = this.completeSections.getPosition( e.target.innerText.trim() );

		let whereTo = HOME_OPTIONS[pos].split(" ").join("_");

		//sets source path for 'GoBack' button to know where to go when fired
		this.currentForm.setSource( this.location.path() );
		this.modal.setAction("next");
		
		//==================//==================//==================//==================
		// console.log(".........................");
		// console.log( this.currentForm.getDirty() );
		/*console.log( this.currentForm.getIsSave() );
		if(!this.currentForm.getIsSave()){
			for(let field in this.currentForm.controls){
				console.log(this.currentForm.controls[field].pristine);
				if(!this.currentForm.controls[field].pristine){
					this.currentForm.setDirty(true);
				}
			}
		}*/
		//gets form dirty status.... THERE MUST BE A BETTER WAY TO REFRESH/GET form status after submition
		
		let formDirty:boolean = this.currentForm.getDirty();
		console.log( this.currentForm );
		// console.log( "FORM DIRTY? "+formDirty );
		//'jump selection'
		if(flag){
			//sets source so "Go Back" can function conrrectly
			this.currentForm.setSource( this.location.path() );
			//sets path for modal to go next
			this.modal.setPath( whereTo );
			
			//Form has been changed
			console.log(formDirty);
			console.log( this.currentForm );

			if( formDirty ){
				//valid form
				if(this.currentForm.status != "INVALID"){
					this.currentForm.setModalDestination(whereTo);
					this.modal.open();
				}
			//form is clean and no changes have been made
			}else{
				this.router.navigate([whereTo]);
				this.title.changeValue( e.target.innerText.trim()  );
				this.toggleActive(e);
			}
		}else{
			this.currentForm.setSource( this.location.path() );
			//toggles option element when user clicks on different options
			//update service to hide add-picture container
			this.show.updateView( true );

			this.title.changeValue( e.target.innerText.trim()  );
		}

		//toggles active|passive for navidation options
		//need to subscribe to modal click on 'save my information'
		this.modal.issave.subscribe((delta)=>{
			if(delta) this.toggleActive(e);
		});
		
	}

	toggleActive( e?, whichOption? ){
		if(e!=null){
			//converts DOM array-like to Array[] type to be able to apply Array functions...
			let options = Array.from(document.getElementsByClassName('navbar-option') );
			//loops through all options and check if any contains 'are-here' class and removes it.... 
			options.map((option)=>{
				if(option.classList.contains('are-here'))
					option.classList.remove('are-here');
			});
			//sets click opton with 'are-here'
			e.target.classList.add('are-here');	
		}else{
			//very weird that I cannot use same implementation as above...Array.from return empty array even though, I can see the array-like
			let pos:number = this.completeSections.getPosition( whichOption.split("/")[1] );
			// converts array-like to Array[] type
			let options = Array.from( document.getElementsByClassName('navbar-option') );
			//sets classname to the route loaded
			options[pos].classList.add('are-here');
		}
	}
}