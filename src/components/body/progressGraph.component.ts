import {Component} from '@angular/core';
import {Subscription} from 'rxjs/Subscription';
// import { CompleteSections } from '../../services/completedSections.service';

@Component({
	selector: 'progress-graph',
	templateUrl: '../../assets/templates/body/progressGraph.template.html',
	styleUrls: ['../../assets/styles/sass/progressGraph.scss']
})export class ProgressGraph{
	subscription: Subscription;
	value:any = 0;
	TOTAL:number = 100;
	sofar:number=0;

	constructor(/* private completed:CompleteSections*/ ){}
	
	add(val=20){
		//It starts all filled with grey and then it needs to refilled with diff color
		this.value = (this.TOTAL - val);
		
		let circle = document.getElementById("bar");

		let r:any = circle.getAttribute("r");
		
		let c = Math.PI*(r*2);
		
		let ptc = c*( (this.TOTAL - this.value )/this.TOTAL );

		//for some reason the view has a string value of empty instead of a string of "0"
		if( circle.style.strokeDashoffset=="")	circle.style.strokeDashoffset = "0";

		circle.style.strokeDashoffset = (parseInt(circle.style.strokeDashoffset)+ptc).toString();
		
		this.sofar += (this.TOTAL - this.value);
	}
	remove(){
		console.log('less');
	}
}