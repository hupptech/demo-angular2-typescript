import {Component} from '@angular/core';
import { Router} from '@angular/router';
import { Location } from '@angular/common';

@Component({
	selector: "add-picture",
	templateUrl: '../../assets/templates/addPicture.template.html',
	styleUrls: ['../../assets/styles/css/addPicture.css']
})export class AddPicture{
	isPath:boolean;
	constructor(private router: Router, private location:Location){
		this.isPath = ( this.location.path() =="" )?true:false;
	}
}