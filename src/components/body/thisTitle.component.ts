import { Component, Inject, Injectable } from '@angular/core';
import { TitleService } from '../../services/thisTitle.service';
import { Subscription } from 'rxjs/Subscription';
import { Router } from '@angular/router';

@Component({
	selector: 'this-title',
	templateUrl: '../../assets/templates/body/thisTitle.template.html',
	styleUrls: ['../../assets/styles/css/personalInformation.css']
}) export class ThisTitle{
	title:string = "";
	subscription: Subscription;
	
	constructor( private titleService:TitleService, private router:Router ) {}

	ngOnInit(){
		this.subscription = this.titleService.thisTitle.subscribe( t=>{
			this.title = t;
		});
	}
}