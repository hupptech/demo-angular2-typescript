import { Component, ViewEncapsulation } from '@angular/core';
import { Router } from '@angular/router';
import { ShowDivs }from '../../services/showDivs.service';
import { CurrentForm } from '../../services/currentForm.service';
// import { MODAL_DIRECTIVES, ModalComponent } from 'ng2-bs3-modal/ng2-bs3-modal';
import { ModalService } from '../../services/modalService.service';
import { User } from '../../services/httpServices/user.service';
import { Location } from '@angular/common';

@Component({
	selector: 'go-back',
	templateUrl: '../../assets/templates/body/goback.template.html'
})export class GoBack{
	isValid:string= "";

	constructor(
		private router:Router
		,private currentForm:CurrentForm
		,private modal:ModalService
		,private location:Location
		,private user:User
	){}

    goBack(){
    	let errors = [];
    	let isPristine:boolean = true;
    	
		//find if form has changed and/or form is pristine or no
		for(let field in this.currentForm.controls){
			if(!this.currentForm.controls[field].pristine){
				errors.push[field];
				isPristine = false;
			}
		}

		if( this.currentForm.status=="VALID" || this.currentForm.controls == null ){
			//if pristine is false then open modal to save changes
			if(!isPristine){
				this.currentForm.setModalDestination("home");
				this.modal.open();
			}else{
				//if there are no changes then navigate directly to the dest card
				this.router.navigate([ '' ]);	
			}
		}else{
			console.log("form has errors");
		}
    }
}
