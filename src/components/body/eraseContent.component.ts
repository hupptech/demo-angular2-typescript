/*
	TODO
		+ when clicked, fire focus event on input that has been applied
*/
import {Component} from '@angular/core';

@Component({
	selector: 'erase-content',
	template: '<button class="eraser icon icon-white xclose notext gray  hide-erase-comp">x</button>'
})export class EraseContent{}