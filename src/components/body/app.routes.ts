import { RouterModule, Routes } from '@angular/router';
import { PersonalInformation } from './../options/personalInformation.component';
import { EducationView } from './../../authorized/education/educationView.component';
import { EducationEdit } from './../../authorized/education/educationEdit.component';
import { ProfessionalExperience } from './../options/professionalExperience.component';
import { EmploymentPreferences } from './../options/employmentPreferences.component';
import { PrivacySettings} from './../options/privacySettings.component';
import { AppComponent } from './app.component';
import { EditExperience } from './../options/editExperience.component';
import { TrainingCertifications } from './../options/trainingCertification.component';

// import { Modal } from './modal.component';

/*
 when dealing with children views, the first children is the default path/component
 HEre is where deafult path '' and component should be initiazed
 */

export const routes: Routes = [
	{
		path: ''
		,children: [
		{
			path: '',
			component: AppComponent
		}
		,{
			path: 'Personal_Information'
			,component: PersonalInformation }
		,{
			path: 'Experience'
			, children: [
				//default view, displays all experiences
				{
					path: '',
					component: ProfessionalExperience
				}
				//creates experience
				,{
					path: 'Create-Experience/:action'
					,component: EditExperience
				}
				//edits experience
				,{
					path: 'Edit-Experience/:id'
					,component: EditExperience
				}
			]

		}
		,{
			path: 'Education'
			, children: [
				{
					path: '',
					component: EducationView
				}
				,{
					path: 'Education_Edit'
					,component: EducationEdit
				},
			]
		}
		,{ path: 'Employment_Preferences', component: EmploymentPreferences }
		,{ path: 'Privacy_Settings', component: PrivacySettings }
		,{ path: "Training_and_Certifications", component: TrainingCertifications }
	]
	}
];

// export const APP_ROUTER_PROVIDERS = [];

export const routing = RouterModule.forRoot(routes);
