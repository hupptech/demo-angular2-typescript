import { Component } from '@angular/core';

import { ModalComponent } from 'ng2-bs3-modal/ng2-bs3-modal';
import { Location } from '@angular/common';

import { Body } from './body.component';
import { Header } from '../header/header.component';
import { Footer } from '../footer/footer.component';

import { SessionCtrls } from './sessionCtrls.component';

// import { SetTemplate } from '../../services/setTemplate.service';
import { AddPicture } from './AddPicture.component';
// import { Subscription } from 'rxjs/Subscription';
import { ShowDivs }from '../../services/showDivs.service';
import { ProgressGraph } from './progressGraph.component';
import { PersonalInformation } from '../options/personalInformation.component';
//import { EducationView } from '../../authorized/education/educationView.component';
//import { EducationEdit } from '../../authorized/education/educationEdit.component';
//need modal component to initialized so it set Element with modal [obj]
import {Modal} from './modal.component';

import { AddRemoveClass } from '../../services/addRemoveClass.service';
import { User } from '../../services/httpServices/user.service';
import { CurrentInfo } from '../../services/currentInfo.service';
import { TitleService } from '../../services/thisTitle.service';

@Component({
	selector: 'landing-site',
	templateUrl: '../../assets/templates/body/app.template.html',
	styleUrls: ['../../assets/styles/css/home-page.css']
	
	,providers: [ AddRemoveClass, TitleService]
})export class AppComponent {
	isHome:boolean;
	thisTitle;
	
	constructor(
		private show:ShowDivs, 
		private location:Location,
		private title:TitleService,
		private user:User,
		private currentInfo:CurrentInfo
	){
		
		// gets user information API
		//info coming from REgistration
		this.user.getUser().subscribe(res=>{
			this.currentInfo.setCurrentInfo( res );
		});
	}

	ngOnInit(){
		//sets correct ishome value for divs to hide/show correctly
		this.show.isShow.subscribe(val=>{
			this.isHome = val;
		});
		
		// sets correct title, based on the route Ex, localhots:3003/Persona_Information would be set to
		// Personal Information
		this.thisTitle = this.title.changeValue( this.location.path().replace(/[/_]/g, ' ').trim() );

		//if view is home, then we update our view to show/hide divs correctly
		if(this.location.path() == "")	this.show.updateView(false);
		else 							this.show.updateView(true);
		
	}
}