import { Component, ViewChild, Output } from '@angular/core';
import { ModalComponent } from 'ng2-bs3-modal/ng2-bs3-modal';
import { ModalService } from '../../services/modalService.service';
import { Router } from '@angular/router';
// import { ShowDivs }from '../../services/showDivs.service';
import { ValidationService } from '../../services/validationService.service';
import { CurrentForm } from '../../services/currentForm.service';
// import { FireValidation } from '../../services/fireValidation.service';
import { ErrorService } from '../../services/error.service';
import { CompleteSections } from '../../services/completeSections.service';
import { Location } from '@angular/common';
import { User } from '../../services/httpServices/user.service';
import { ExperienceService as Experience } from '../../services/httpServices/experience.service';
import { EducationService } from '../../services/httpServices/education.service';

@Component({
	selector: 'my-modal',
	template: `
		<modal #modal>
			<modal-header [show-close]="true">
				<h4 class="modal-title">{{modalContent.title}}</h4>
			</modal-header>
			<modal-body>
				{{modalContent.body}}
			</modal-body>
			<modal-footer [show-default-buttons]="false">
				<button
					#ok
					type="button" 
					(click)="saveInformation()"
				>{{modalContent.buttons.ok}}</button>
				<button
					#cancel
					type="button" 
					(click)="noSaving()"
				>{{modalContent.buttons.cancel}}</button>
			</modal-footer>
		</modal>
	`
})export class Modal{
	@ViewChild('modal') modal:ModalComponent;
	path:string = "";
	@Output() modalContent = {
		title : ""
		,body: ""
		,buttons: {
			ok: ""
			,cancel: ""
		}
	};

	constructor(
			// private thismodal:ModalService
			private router:Router
			,private curr:CurrentForm
			,private error:ErrorService
			,private completeSections:CompleteSections
			,private location:Location
			,private modalservice:ModalService
			,private user:User
			,private experience:Experience
	){
		//subscribe to event to check if modal content could change in diff views
		this.modalservice.thiscontent.subscribe((modal)=>{
			if(modal!=""){
				this.modalservice.setElement( this.modal );
				this.modalContent = modal;
			}
		});
	}

	ngOnInit(){
		this.modalservice.setDefault();
	}
	//This was updated on angular2 final release...before ngAfterViewInit would have picked up
	// the modal element and set it to the current modal to use....
	// seems like afterviewChecked is the right hook to instantiate modal
	ngAfterViewChecked(){
		this.modalservice.setElement(this.modal);
	}

	saveInformation(){
		//gets what the ok button should do in a modal
		let whereTo = this.curr.getModalDestination();
		//gets current controls
		let form =  this.curr.getControls();
		//either way... if going back or forward...modal means there are changes to be saved

		//modal router
		if(whereTo == "delete"){
			this.experience.deleteExperience(this.curr.getIdToDelete()).subscribe((res)=>{
				console.log(res);
			});
		}else if(whereTo == "next"){
			//write a better regex so it only requires one replace
			let cardName:string = this.location.path().split("/")[1].replace(/_/g, '').replace(/\//, '');
			//get position of current card
			// console.log(cardName);
			//call position+1 in the completedSection in completeSEction.service
			let pos = this.completeSections.getPosition( cardName );
			//gets name out of the object {name:"Experience", value: 'false'}
			let nextCard = this.completeSections.completedSections[(pos+1)].name;
			//sets value to true for current card
			this.completeSections.completedSections[pos].value = true;
			// console.log(form);
			let location = this.getLocation( this.location.path() );

			switch(location){
				case 'Experience':
					// console.log( this.currentForm );
					if(this.location.path().indexOf('Edit')>0){
						this.experience.updateExperience( form, this.location.path().split("/")[3] );
						this.router.navigate([nextCard]);
					//will create experience
					}else if(this.location.path().indexOf('Create')>0){
						this.experience.createExperience( form );
						this.router.navigate([nextCard]);
					}
				break;
				case 'PersonalInformation':
					this.user.setUserObj( form );
					this.user.postUser();
					this.router.navigate([nextCard]);
				break;
			}
			
		}else if(whereTo == "home"){
			//"go back" router
			
			let location = this.getLocation( this.location.path() );
			switch(location){
				case 'Experience':
					// console.log( this.currentForm );
					if(this.location.path().indexOf('Edit')>0){
						this.experience.updateExperience( form, this.location.path().split("/")[3] );
						this.router.navigate(['']);	
					//will create experience
					}else if(this.location.path().indexOf('Create')>0){
						this.experience.createExperience( form );
						this.router.navigate(['']);
					}
				break;
				case 'PersonalInformation':
					this.user.setUserObj( form );
					this.user.postUser();
					this.router.navigate(['']);
				break;
			}
		//means that user wants to go to another destination in navigation bar besides home, next
		}else{
			let location = this.getLocation( this.location.path() );
			switch(location){
				case 'Experience':
					// console.log( this.currentForm );
					if(this.location.path().indexOf('Edit')>0){
						this.experience.updateExperience( form, this.location.path().split("/")[3] );
						this.router.navigate([whereTo]);	
					//will create experience
					}else if(this.location.path().indexOf('Create')>0){
						this.experience.createExperience( form );
						this.router.navigate([whereTo]);
					}
				break;
				case 'PersonalInformation':
					this.user.setUserObj( form );
					this.user.postUser();
					this.router.navigate([whereTo]);
				break;
			}
		}
		this.modalservice.close();
		this.modalservice.contentSave(true);
	}

	noSaving(){
		// SHOULD NOT SAVE INFORMATION
		this.router.navigate([ '' ]);
	}

	ngOnDestory(){
		console.log("modal being destroyed!!!");
	}

	getLocation(path){
		if(path.indexOf("Experience")>0){
			return 'Experience';
		}else if(path.indexOf('Personal_Information')>0){
			return "PersonalInformation"
		}
	}
}