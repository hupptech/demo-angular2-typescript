import {Component} from '@angular/core';


@Component({
	selector: 'header',
	templateUrl: '../../../assets/templates/header/header.template.html',
	styleUrls: ['../../../assets/styles/css/header/header.css']
}) export class Header{}