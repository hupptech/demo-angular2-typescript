/*import { Component, OnChanges, OnInit } from '@angular/core';
// import { MultiSelect2 } from '../multiSelect2/multiSelect2.component';
import { FIELDS_OF_PRACTICE } from '../../constants/fops2.constants';
import { DISCIPLINES } from '../../constants/disciplines2.constants';
import { SPECIALTIES } from '../../constants/specialties.constant';
import * as _ from 'lodash';

@Component({
	selector: "personalTaxonomySelect",
	template: `
		<multi-select2 
			[title]="'Fields of Practice'" 
			[help]="'fldPractice'"
			[list]="fopList" 
			(onChange)="updateDisciplinesByFoP($event)">
		</multi-select2>
		<multi-select2 
			*ngIf="disciplineList.length > 0" 
			[list]="disciplineList"
			[title]="'Disciplines'"
			[help]="'default'"
			(onChange)="updateSpecialtyByDisciplines($event)">
		</multi-select2>
		<multi-select2 
			*ngIf="specialtiesList.length > 0" 
			[list]="specialtiesList" 
			[title]="'Specialty'"
			[help]="'default'">
		 </multi-select2>
	`,
	directives: [MultiSelect2]
})
export class PersonalTaxonomySelect implements OnInit, OnChanges {
    fopList = [];
    disciplineList = [];
    specialtiesList = [];
    constuctor(){}

    ngOnInit() {
      this.initializeWithNoIncomingData();
    }
  
    ngOnChanges() {
      // if(_.isUndefined(this.incomingData) || this.incomingData.length  === 0) {
      //   this.initializeWithNoIncomingData();
      // } else {
      //   this.intializeWithData();
      // }
    }
  
    updateDisciplinesByFoP (list:Array<String>) {
        // check if empty; if so, both discipline and specialties are empty
        if(list.length === 0) {
            this.disciplineList = [];
            this.specialtiesList = [];
        } else {
            // this is awful
            let newList = [];
            // create new list of disciplines from constant based on chosen fops
            list.forEach((fop) => {
              DISCIPLINES.forEach((discipline) => {
                if(discipline.associatedFieldOfPractice === fop) {
                  newList.push(discipline);
                }
              });
            });
            // check to see if the items from the old list were checked and assign back accordingly
            newList.forEach((newItem) => {
              this.disciplineList.forEach(function(oldItem) {
                  if(newItem.id === oldItem.id) {
                    newItem.isChecked = oldItem.isChecked;
                  }
              });
            });
            this.disciplineList = this.buildMultiSelectItemsFromList(newList);
        }
    }
	
	 updateSpecialtyByDisciplines (list) {
      if(list.length === 0) {
          this.specialtiesList = [];
      } else {
        // to implement: filter by discipline
        this.specialtiesList = this.buildMultiSelectItemsFromList(SPECIALTIES);
      }
    }

    buildMultiSelectItemsFromList(list) {
      let multiSelectItems = [];
      list.forEach(function(item) {
          let obj = {
            id: item.id,
            label: item.label,
            isChecked: item.isChecked ? true : false
          };
          multiSelectItems.push(obj);
      });
      return multiSelectItems;
    }

    initializeWithNoIncomingData() {
      this.fopList = this.buildMultiSelectItemsFromList(FIELDS_OF_PRACTICE);
    }

    intializeWithData() {
        // to implement
    }

    
}*/