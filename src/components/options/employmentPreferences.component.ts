import {Component, ElementRef, EventEmitter, Output, Input} from '@angular/core';
import { EmploymentPreferencesService } from '../../services/employmentPreferences.service';
import { OnInit } from '@angular/core';
import { Location } from '../../utils/location';
import { Router } from '@angular/router';




export class Preferences {
  rural: boolean;
  urban: boolean;
  fullTime: boolean;
  partTime: boolean;
  preferences: boolean;
  relocate: boolean;
}


@Component({
	selector:'employment-preferences',
	templateUrl: '../../assets/templates/body/options/employmentPreferences.template.html',
	styleUrls: ['../../assets/styles/css/content.css','../../assets/styles/css/form.css'],
	providers: [EmploymentPreferencesService]
  	
	
}) export class EmploymentPreferences implements OnInit {


	 
	locations : Location[];
	
	dropdownItems : number[];
	
	locationsCount : number;
	
	//dropDown Properties 
	inputName: 'Select Location';
   	inputValue : '';
    dropdownVisible : boolean; //false
    allLocations : boolean; //true
    selectLocationVisible: boolean; //false
    pressedDropdown = false;

    
    //end DropDown Properties
	
	@Output() preferences : Preferences  = { rural: false, urban : false, fullTime : false, partTime : false,  preferences:false, relocate:true};
	
	constructor(private employmentPreferencesService: EmploymentPreferencesService, private router:Router) {

		this.dropdownVisible = false;
		this.allLocations = true;
		this.selectLocationVisible = false;
	
	};
	
	getLocations() : void { 
		
		this.employmentPreferencesService.getLocations().then(locations => {this.locations = locations, 
		this.dropdownItems = Object.assign([], locations), this.locationsCount = this.locations.length});
		
	}
	
	
	SetRural(){
	
		if(this.preferences.rural){
			
			this.preferences.rural = false;
		
		}else{
			
			this.preferences.rural = true;
			
		}
	}
	
	SetUrban(){
	
		if(this.preferences.urban){
			
			this.preferences.urban = false;
		
		}else{
			
			this.preferences.urban = true;
			
		}
	}
	
	SetFullTime(){
	
		if(this.preferences.fullTime){
			
			this.preferences.fullTime = false;
		
		}else{
			
			this.preferences.fullTime = true;
			
		}
	}
	
	SetPartTime(){
	
		if(this.preferences.partTime){
			
			this.preferences.partTime = false;
		
		}else{
			
			this.preferences.partTime = true;
			
		}
	}
	
	SetPreferences(){
	
		if(this.preferences.preferences){
			
			this.preferences.preferences = false;
		
		}else{
			
			this.preferences.preferences = true;
			this.preferences.partTime = false;
			this.preferences.fullTime = false;
			this.preferences.rural = false;
			this.preferences.urban = false;
			
			
		}
	}

	
	ngOnInit(): void {
   		this.getLocations();
   		this.dropdownItems = [];
  	}

     inputFocus() : void {
    
        this.showDropdown();
      }

      inputBlur() : void {
      
        if (this.pressedDropdown) {
          // Blur event is triggered before click event, which means a click on a dropdown item wont be triggered if we hide the dropdown list here.
          this.pressedDropdown = false;
          return;
        }
        this.hideDropdown();
      };

      dropdownPressed() : void {
        this.pressedDropdown = true;
      }

      addRemoveLocation(location) : void {
      	var index = this.dropdownItems.indexOf(location);
      	
      	if(index >= 0) {
      		this.allLocations = false;
      		location.selected = false;
      		this.dropdownItems.splice(index, 1);
      	} else { 
      		location.selected = true;
       		this.dropdownItems.push(location); 
       	}
      
       	 
      }
      
      addRemoveAllLocations() {
      
      	
       var list = this.locations;
      
	      	if( this.allLocations) {
	      		 this.allLocations = false;
	      		 this.dropdownItems = [];
	      		 for(var x in list) {
	      		 	list[x].selected = false;
	      		 }
	      	} else {
	      		this.allLocations = true;
	      		
	      		this.dropdownItems = Object.assign([], list)
	      		for(var x in list) {
	      		 	list[x].selected = true;
	      		 }
	      	}
      
      }

      showDropdown() : void {
        this.dropdownVisible = true;
      }
      hideDropdown() : void {
        this.dropdownVisible = false;
      }
  	
	saveAndContinue(){
		this.router.navigate(['/Privacy_Settings']);
	}

	

}