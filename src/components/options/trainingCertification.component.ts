
//@Component({
//	selector: 'training-certifications',
//	template: '<div> This content for Training and Certifications</div>'
//})export class TrainingCertifications{
//}

//======================================ANGULAR CORE COMPONENTS======================================
import { Component, Input } from '@angular/core';
import { Location } from '@angular/common';
import {Router} from '@angular/router';
// import { MODAL_DIRECTIVES } from 'ng2-bs3-modal/ng2-bs3-modal';
import { FormGroup, FormArray, FormBuilder, Validators, AbstractControl, FormControl } from '@angular/forms';
//======================================COMPONENTS======================================
import { EraseContent } from '../body/eraseContent.component';


import { WordCount } from '../body/wordCount.component';

import { ErrorMessage } from '../body/errorMessages.component';
// import { PersonalTaxonomySelect } from '../personalTaxonomySelect/personalTaxonomySelect.component';

import {RadioControlValueAccessor} from "../body/radio_value_accessor";
//======================================SERVICES======================================
import { TitleService } from  '../../services/thisTitle.service';
import { ValidationService } from '../../services/validationService.service';
import { AddRemoveClass } from '../../services/addRemoveClass.service';
// import { CompleteSections } from '../../services/completedSections.service';
import { ShowDivs } from '../../services/showDivs.service';
import { CurrentForm } from '../../services/currentForm.service';
// import { FireValidation } from '../../services/fireValidation.service';
import { ErrorService } from '../../services/error.service';
import { User } from '../../services/httpServices/user.service';
import { CurrentInfo } from '../../services/currentInfo.service';
import { ModalService } from '../../services/modalService.service';

let wrapper = '../../assets/styles/css/content.css';
let form = "../../assets/styles/css/form.css";
let switchToggle = "../assets/styles/css/switch.css";
let trainingAndCertifications = '../../assets/templates/body/options/trainingAndCertifications.template.html';

interface ResponseObject{
	professionalTitle?:string;
	firstName?:string;
	lastName?:string;
	location?:string;
	npiNumber?:number;
	phoneNumber?:string;
	personalStatement?:string;
	languageProficiencies?:Array<{language:string, proficiency:string}>;
	profileFieldOfPractices?:Array<string>;
	profileDisciplines?:Array<string>;
	profileSpecialties?:Array<string>;
	profileLicenses?: Array<string>;
	profileBhwPrograms?: string;
	email?: string;
	
}

@Component({
	templateUrl: trainingAndCertifications,
	styleUrls: [ wrapper, form, switchToggle ]
})

export class TrainingCertifications{
	// formGroup="";
	public userForm: FormGroup;
	thisTitle:string="";
	counter:number;
	form;
	
	/*
		TODO
		+ needs to be of type Observable so changes can propagate to children component; 
		otherwise, change detection will not detect new values
	*/
	submitAttempt:boolean = false;

	IsTraining: boolean = true;
	isCurrentlyInTraining: boolean = false;
	months: string[] = [ "January", "February", "March", "April", "May", "June",
		"July", "August", "September", "October", "November", "December" ];
	years: string[] = [];
	endDateValidation: string = "";

	constructor(
		private formBuilder: FormBuilder
		,private show:ShowDivs
		,private title: TitleService
		,private ARC:AddRemoveClass
		,private router:Router
		,private currForm:CurrentForm
		// ,private fire:FireValidation
		,private error:ErrorService
		,private user:User
		,private currentInfo: CurrentInfo
		,private modal:ModalService
		,private location:Location
	){
		// console.log("Constructor: "+this.currForm.getDirty() )
		//INITIALIZE FORM TO PASS REFERENCE TO TEMPLATE
		this.userForm = this.formBuilder.group({
			'trainingName': [ '', ValidationService.TrainingName]
			,'fromMonth': ['']
			,'fromYear': ['']
			,'toMonth': ['']
			,'toYear': ['']
			,'isCurrentlyInTraining': ['']
			,'description': ['', ValidationService.Description]
		});
		
		currForm.updateCurrent( this.userForm.status );
		
		
		// console.log(this.userForm.status);

		//SETS REFERENCE USER CONTROLS
		this.currForm.setControls( this.userForm.controls );

		//PREPOPULATE FIELDS WITH ENDPOINT =====
		this.user.getUser().subscribe((res)=>{
			// console.log(res);
		});

		let years = [];
		for(let i=1900;i<new Date().getFullYear()+10;i++){
			years.push(i);
		}
		this.years = years;
	}

	toTitleCase(str){
    	return str.replace(/\w\S*/g, function(txt){ return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();});
	}
	initStateLicense(){
		return this.formBuilder.group({
			"stateLicense": ['', ValidationService.State]
		});
	}
	initLanguage(idx:number){
		//only first instantiation should be required field
		if(idx != 0){
			return this.formBuilder.group({
				'language' : ['', ValidationService.LanguageNotRequired ]
				,'languageProficiency': ['', ValidationService.ProficiencyNotRequired ]
			});	 
		}else{
			//first instantiation of languages Array Form
			return this.formBuilder.group({
				'language' : ['', ValidationService.Language ]
				,'languageProficiency': ['', ValidationService.Proficiency]
			});	
		}
		
	}

	addLanguage(){
		const control = <FormArray>this.userForm.controls['languages'];
		control.push( this.initLanguage(0) );
	}
	addState(){
		const control = <FormArray>this.userForm.controls['stateLicenses'];
		control.push( this.initStateLicense() );
	}

	removeLanguage(i:number){
		const control = <FormArray>this.userForm.controls['languages'];
		control.removeAt(i)
	}
	removeStateLicense(i:number){}

	//erased input content on click/tap
	eraseContent(id){
		this.userForm.controls[id].setValue('');
	}

	toggle(){
		if(!this.currForm.isSave){
			this.modal.setAction(()=>{
				this.IsTraining = !this.IsTraining;
			});
			this.modal.open();
		} else {
			this.IsTraining = !this.IsTraining;
		}
	}

	checkEndDate(){
		setTimeout(()=>{
			this.endDateValidation = "";
			let isNotEmptyYear = this.userForm.controls["toYear"].value!=="" && this.userForm.controls["fromYear"].value !=="";
			if(isNotEmptyYear && this.userForm.controls["toYear"].value < this.userForm.controls["fromYear"].value ){
				this.endDateValidation = "The end date must be after the start date";
			} else {
				let isNotEmptyMonth = this.userForm.controls["toMonth"].value!=="" && this.userForm.controls["fromMonth"].value !=="";
				if(isNotEmptyMonth && this.userForm.controls["toYear"].value === this.userForm.controls["fromYear"].value){
					let lowCaseMonth = this.months.map((month)=>month.toLocaleLowerCase());
					if(lowCaseMonth.indexOf(this.userForm.controls["toMonth"].value) < lowCaseMonth.indexOf(this.userForm.controls["fromMonth"].value)){
						this.endDateValidation = "The end date must be after the start date";
					}
				}
			}
		},0);
	}

	toggleCurrentlyTraining(){
		this.isCurrentlyInTraining = !this.isCurrentlyInTraining;
		if(this.isCurrentlyInTraining){
			this.userForm.controls["toMonth"].setValue('');
			this.userForm.controls["toYear"].setValue('');
		}
	}

	/* Add any logic for when component is mounted */
	ngOnInit(){
		
		this.modal.setDefault();
		
		this.userForm.statusChanges.subscribe(status=>{
			console.log("inside subscriber");
			console.log(status);
			console.log(this.currForm);
			//if any changes happend, setIsSave to false, so that body.component nav bar fires pristine check on form saved
			this.currForm.setIsSave(false);
			// this.currForm.updateCurrent(status);
		});
		this.show.updateView(true);
		//should always be VALID form when it is initializing
	}
	//updates textarea counter for max characters
	UpdateCounter($event){
		if($event.keyCode===8){
			if(this.counter>0) {
				this.counter -= 1;
			}
		} else {
			this.counter = $event.srcElement.textLength+1;
		}
	}
	//toggles class of 'erase-cmp' to show/hide respectively
	showEraseComp(event){
		this.ARC.RemoveClass(event.srcElement.nextElementSibling.firstChild, "hide-erase-comp");
		this.ARC.AddClass(event.srcElement.nextElementSibling.firstChild, "show-erase-comp");
	}
	focusOut(event){
		let that = this;
		// wait 1 ms so event does not disappear
		setTimeout(function(){
			that.ARC.RemoveClass(event.srcElement.nextElementSibling.firstChild, "show-erase-comp");
			that.ARC.AddClass(event.srcElement.nextElementSibling.firstChild, "hide-erase-comp");	
		}, 100);
	}

	addAnother(){
		// this.save();
		for(let field in this.userForm.controls){
			this.userForm.controls[field].setValue('');
		}
	}

	/*
		When this function is invoked...it should:
			+ save changes if no errors and then redirected to next card
			+ if errors present, do nothing -stay in current page
	*/
	saveAndContinue(){
		let errors = [];

		for(let field in this.userForm.controls){
			if( this.userForm.controls[field].status == "INVALID" ){
				errors.push(field);
			}
		}
		//if errors 1 or more
		//scroll to where the first errors is located...work on animation for this...w/o JQuery
		if(errors.length){
			alert('there are some errors, please revise');
		}else{
			/*No Errors but needs to check if form has changed -pristine, no need to show modal...just save changes*/
			if(!this.currForm.getIsSave()){
				//set destination for modal 'save and continue' to go to next card
				this.currForm.setModalDestination("next");
				//========= POSSIBLE COMBINE THESE TWO INTO ONE CALL TO SET OBJ AND TO POST
				//sets obj to be posted
				this.user.setUserObj( this.userForm.controls );
				//posts new data
				this.user.postUser();
				//========================================================================
				//sets observable isSAve to true so other functions know that data has been saved
				this.currForm.setIsSave(true);
			}
			//if data has been save and there is no data to be saved, redirects to next card
			this.router.navigate( ['/Experience'] );
			//sets form dirty to false...no new changes have been made
			this.currForm.setDirty(false);
		}

	}

	/*
		when component being destroy, set Source to current path, so "go back" knows where to route is clicked.
	*/
	ngOnDestroy(){
		// console.log("Personal Information about to be destroyed...");
		// console.log("Setting source path");
		this.currForm.setSource( this.location.path() )
	}
}