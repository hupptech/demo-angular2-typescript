//======================================ANGULAR CORE COMPONENTS======================================
import { Component, Input } from '@angular/core';
import { Location } from '@angular/common';
import {Router} from '@angular/router';
// import { MODAL_DIRECTIVES } from 'ng2-bs3-modal/ng2-bs3-modal';
import { FormGroup, FormArray, FormBuilder, Validators, AbstractControl, FormControl } from '@angular/forms';
//======================================COMPONENTS======================================
import { EraseContent } from '../body/eraseContent.component';


import { WordCount } from '../body/wordCount.component';

import { ErrorMessage } from '../body/errorMessages.component';
// import { PersonalTaxonomySelect } from '../personalTaxonomySelect/personalTaxonomySelect.component';
//======================================SERVICES======================================
import { TitleService } from  '../../services/thisTitle.service';
import { ValidationService } from '../../services/validationService.service';
import { AddRemoveClass } from '../../services/addRemoveClass.service';
// import { CompleteSections } from '../../services/completedSections.service';
import { ShowDivs } from '../../services/showDivs.service';
import { CurrentForm } from '../../services/currentForm.service';
// import { FireValidation } from '../../services/fireValidation.service';
import { ErrorService } from '../../services/error.service';
import { User } from '../../services/httpServices/user.service';
import { CurrentInfo } from '../../services/currentInfo.service';
import { ModalService } from '../../services/modalService.service';

let wrapper = '../../assets/styles/css/content.css';
let form = "../../assets/styles/css/form.css";
let switchToggle = "../assets/styles/css/switch.css";
let personalInformationTemplate = '../../assets/templates/body/options/personalInformation.template.html';

interface ResponseObject{
	professionalTitle?:string;
	firstName?:string;
	lastName?:string;
	location?:string;
	npiNumber?:number;
	phoneNumber?:string;
	personalStatement?:string;
	languageProficiencies?:Array<{language:string, proficiency:string}>;
	profileFieldOfPractices?:Array<string>;
	profileDisciplines?:Array<string>;
	profileSpecialties?:Array<string>;
	profileLicenses?: Array<string>;
	profileBhwPrograms?: string;
	email?: string;
	
}

@Component({
	templateUrl: personalInformationTemplate,
	styleUrls: [ wrapper, form, switchToggle ]
})export class PersonalInformation{
	// formGroup="";
	public userForm: FormGroup;
	thisTitle:string="";
	counter:number;
	form;
	
	/*
		TODO
		+ needs to be of type Observable so changes can propagate to children component; 
		otherwise, change detection will not detect new values
	*/
	submitAttempt:boolean = false;

	constructor(
		private formBuilder: FormBuilder
		,private show:ShowDivs
		,private title: TitleService
		,private ARC:AddRemoveClass
		,private router:Router
		,private currForm:CurrentForm
		// ,private fire:FireValidation
		,private error:ErrorService
		,private user:User
		,private currentInfo: CurrentInfo
		,private modal:ModalService
		,private location:Location
	){
		// console.log("Constructor: "+this.currForm.getDirty() )
		//INITIALIZE FORM TO PASS REFERENCE TO TEMPLATE
		this.userForm = this.formBuilder.group({
			'firstname': [ '' , ValidationService.Firstname]
			,'lastname': [ '', ValidationService.Lastname ]
			,'title': [ '', ValidationService.Title ]
			,'location': [ '', ValidationService.Location ]
			,'bhwProgram': ['']
			// ,'fldPractice': ['']
			,'profileFieldOfPractices': ['']
			,'profileDisciplines' : ['']
			,'profileSpecialties' : ['']
			,'languages' : this.formBuilder.array([
				this.initLanguage(0)
			])
			,'email': ['']
			,'phone': [ '', ValidationService.Phone ]
			,'npi': [ '', /*Validators.required*/ ]
			,'stateLicenses': this.formBuilder.array([
				this.initStateLicense()
			])
			,'statement': [ '', ValidationService.Statement ]
		});
		
		currForm.updateCurrent( this.userForm.status );
		
		
		// console.log(this.userForm.status);

		//SETS REFERENCE USER CONTROLS
		this.currForm.setControls( this.userForm.controls );

		//PREPOPULATE FIELDS WITH ENDPOINT =====
		this.user.getUser().subscribe((res)=>{
			// console.log(res);
			/*
				TODO
				Add the following masking to a filter
			*/
			let masked = [];
			res.phoneNumber.split('').map((ch, idx)=>{
				if(idx == 2 || idx == 5 ){
					masked.push(ch);
					masked.push('-');
				}else 	masked.push(ch);
			});
			console.log("==============");
			console.log( this.currForm );
			this.userForm.controls['firstname'].setValue( res.firstName );
			this.userForm.controls['lastname'].setValue( res.lastName );
			this.userForm.controls['npi'].setValue( res.npiNumber );
			this.userForm.controls['phone'].setValue( masked.join("") );
			this.userForm.controls['statement'].setValue( res.personalStatement );
			this.userForm.controls['email'].setValue( (!res)?res.email:'' );
			this.userForm.controls['location'].setValue( res.location );
			this.userForm.controls['title'].setValue( res.professionalTitle );
			console.log( this.currForm );
			/*
				APPLY A FILTER TO RESPONSED FROM API THAT ARE IN UPPERCASE. -First Character should only be UPPERCASE, the rest lowecase
			*/

			let langArr = [];
			for(var i=0; i<res.languageProficiencies.length; i++){

				langArr.push(this.initLanguage(i));
			}
			this.userForm.controls['languages'] = this.formBuilder.array( langArr );

			for(var i=0; i<res.languageProficiencies.length; i++){
				(<FormArray>(<FormGroup>this.userForm.controls['languages']).controls[i]).controls["language"].setValue( this.toTitleCase(res.languageProficiencies[i].language ) );
				(<FormArray>(<FormGroup>this.userForm.controls['languages']).controls[i]).controls["languageProficiency"].setValue( res.languageProficiencies[i].proficiency );
			}

			let arr = [];
			//pushes instantiations of form formGroup into an empty array
			for(var i=0; i<res.profileLicenses.length; i++){
				arr.push(this.initStateLicense());
			}	
			//adds an instance of formBuilder.array with the previous array of formGroups
			this.userForm.controls['stateLicenses'] = this.formBuilder.array( arr );
			//loops thought all states in res and sets its correct value to the array instance of formGroup
			for(var i=0; i<res.profileLicenses.length; i++){
				res.profileLicenses[i] = res.profileLicenses[i].split("_").join(" ");
				(<FormArray>(<FormGroup>this.userForm.controls['stateLicenses']).controls[i]).controls['stateLicense'].setValue( this.toTitleCase(res.profileLicenses[i]) );	
			}
		});
	}

	toTitleCase(str){
    	return str.replace(/\w\S*/g, function(txt){ return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();});
	}
	initStateLicense(){
		return this.formBuilder.group({
			"stateLicense": ['', ValidationService.State]
		});
	}
	initLanguage(idx:number){
		//only first instantiation should be required field
		if(idx != 0){
			return this.formBuilder.group({
				'language' : ['', ValidationService.LanguageNotRequired ]
				,'languageProficiency': ['', ValidationService.ProficiencyNotRequired ]
			});	 
		}else{
			//first instantiation of languages Array Form
			return this.formBuilder.group({
				'language' : ['', ValidationService.Language ]
				,'languageProficiency': ['', ValidationService.Proficiency]
			});	
		}
		
	}

	addLanguage(){
		const control = <FormArray>this.userForm.controls['languages'];
		control.push( this.initLanguage(0) );
	}
	addState(){
		const control = <FormArray>this.userForm.controls['stateLicenses'];
		control.push( this.initStateLicense() );
	}

	removeLanguage(i:number){
		const control = <FormArray>this.userForm.controls['languages'];
		control.removeAt(i)
	}
	removeStateLicense(i:number){}

	//erased input content on click/tap
	eraseContent(id){
		this.userForm.controls[id].setValue('');
	}
	toggle(value){
		if(value.srcElement.value=="on")	value.srcElement.value = "off";
		else 								value.srcElement.value = "on";
	}
	/* Add any logic for when component is mounted */
	ngOnInit(){
		
		this.modal.setDefault();
		
		this.userForm.statusChanges.subscribe(status=>{
			console.log("inside subscriber");
			console.log(status);
			console.log(this.currForm);
			//if any changes happend, setIsSave to false, so that body.component nav bar fires pristine check on form saved
			this.currForm.setIsSave(false);
			// this.currForm.updateCurrent(status);
		});
		this.show.updateView(true);
		//should always be VALID form when it is initializing
	}
	//updates textarea counter for max characters
	UpdateCounter($event){
		($event.keyCode==8)?this.counter -= 1:this.counter = $event.srcElement.textLength+1;
	}
	//toggles class of 'erase-cmp' to show/hide respectively
	showEraseComp(event){
		this.ARC.RemoveClass(event.srcElement.nextElementSibling.firstChild, "hide-erase-comp");
		this.ARC.AddClass(event.srcElement.nextElementSibling.firstChild, "show-erase-comp");
	}
	focusOut(event){
		let that = this;
		// wait 1 ms so event does not disappear
		setTimeout(function(){
			that.ARC.RemoveClass(event.srcElement.nextElementSibling.firstChild, "show-erase-comp");
			that.ARC.AddClass(event.srcElement.nextElementSibling.firstChild, "hide-erase-comp");	
		}, 100);
	}

	/*
		When this function is invoked...it should:
			+ save changes if no errors and then redirected to next card
			+ if errors present, do nothing -stay in current page
	*/
	saveAndContinue(obj){
		let errors = [];
		
		for(let field in this.userForm.controls){
			if( this.userForm.controls[field].status == "INVALID" ){
				errors.push(field);
			}
		}
		//if errors 1 or more 
		//scroll to where the first errors is located...work on animation for this...w/o JQuery
		if(errors.length){
			alert('there are some errors, please revise');
		}else{
			/*No Errors but needs to check if form has changed -pristine, no need to show modal...just save changes*/
			if(!this.currForm.getIsSave()){
				//set destination for modal 'save and continue' to go to next card
				this.currForm.setModalDestination("next");
				//========= POSSIBLE COMBINE THESE TWO INTO ONE CALL TO SET OBJ AND TO POST
				//sets obj to be posted
				this.user.setUserObj( this.userForm.controls );
				//posts new data
				this.user.postUser();
				//========================================================================
				//sets observable isSAve to true so other functions know that data has been saved
				this.currForm.setIsSave(true);
			}
			//if data has been save and there is no data to be saved, redirects to next card
			this.router.navigate( ['/Experience'] );
			//sets form dirty to false...no new changes have been made
			this.currForm.setDirty(false);
		}

	}

	/*
		when component being destroy, set Source to current path, so "go back" knows where to route is clicked.
	*/
	ngOnDestroy(){
		// console.log("Personal Information about to be destroyed...");
		// console.log("Setting source path");
		this.currForm.setSource( this.location.path() )
	}
}