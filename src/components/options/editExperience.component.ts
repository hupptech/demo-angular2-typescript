import { Component, Output, Directive } from '@angular/core';
import { Location } from '@angular/common';
import { EraseContent } from '../body/eraseContent.component';
import { FormGroup, FormArray, FormBuilder, Validators } from '@angular/forms';
import { CurrentForm } from '../../services/currentForm.service';
import { ValidationService } from '../../services/validationService.service';
import { ErrorMessage } from '../body/errorMessages.component';
import { MONTHS } from '../../constants/months.constant';
import { AddRemoveClass } from '../../services/addRemoveClass.service';

import { ExperienceService as Experience } from '../../services/httpServices/experience.service';
import { Router } from '@angular/router';
// import { CurrentInfo } from '../../services/currentInfo.service';

let switchToggle = "../../assets/styles/css/switch.css";
let editExperience = "../../assets/styles/css/editExperience.css"

interface ExperienceObj{
	title?:string,
	organization?:string,
	location?:string,
	fromTimePeriod?:Array<{fromMonth:string, fromYear:number}>,
	toTimePeriod?:Array<{toMonth:string, toYear:number}>,
	description?: string,
	isResidency?:boolean,
	isWorkingHere?:boolean
}
	
@Component({
	selector: 'edit-experience',
	templateUrl: '../../assets/templates/body/options/editExperience.template.html',
	styleUrls: [ switchToggle, editExperience ],
	styles:[`
		.thisEraseContent{
			position:absolute;
			top:54px;
			right: 0px;
		}
	`]
})export class EditExperience{
	
	public experienceForm: FormGroup; //saves instance for form to be passed to template
	@Output() months:Array<string> = [""]; //store array of months for dropdown
	@Output() years: Array<number> = []; //stores array of year for dropdown
	// @Output() toggleEraseContent:boolean = true; //will toggle visibility for eraseComponent
	//properties that are different among component instantiation, should be in an object
	//otherwise variable will be global thoughtout every component
	//keeping it 
	@Output() private values = {
		isResidency: {text: "OFF", value: false}
		,isWorkingHere: {text: "OFF", value: false}
		,eraseContent: { value: true}
	}
	@Output() disabledYears = false; //sets years to be disable when 'toYear' conflicts
	startIndex:number;
	counter:number;
	thisExperience:ExperienceObj;

	constructor(
		private formBuilder:FormBuilder
		,private currentForm:CurrentForm
		,private location:Location
		,private ARC:AddRemoveClass
		,private experience:Experience
		,private router:Router
	){
		this.experienceForm = this.formBuilder.group({

			'title': ['', ValidationService.Title ]
			,'organization': ['', ValidationService.Organization]
			,'location': ['', ValidationService.Location]
			,'fromTimePeriod': this.formBuilder.group({
				"fromMonth" : ['']
				,"fromYear": [0]
			})
			,'toTimePeriod': this.formBuilder.group({
				'toMonth': ['']
				,'toYear': [0]
			})
			,"description": ['', ValidationService.Statement ]
			,"isResidency": [false]
			,"isWorkingHere": [false]
		});		
	}

	ngOnInit(){
		//it is an edit action
		if(this.location.path().indexOf('Edit')>0){
			//split path and send 3 index which is the id
			this.experience.getExperience(this.location.path().split("/")[3]).subscribe(
				(data)=>{
					console.log( data.json() );
					this.thisExperience = data.json();
					this.setFormValues( this.thisExperience);
				},
				(err)=> console.log(err),
				()=> console.log("GET completed")
			);
			
		}

		/*
			Add a checker to see if user has click on 'edit' or 'add another'
		*/
		this.months = MONTHS;
		//gets current date...
		let currentDate = new Date();
		let currentYear = currentDate.getFullYear();
		//========THERE MUST BE A BETTER WAY TO DO THIS IN THE HTML/ANGULAR=====
		//PLEASE FIND IT SOON...
		for(let year=1900; year<=(currentYear+10); year++)	this.years.push(year);
		
		//set dirty property to false, brand new form
		this.currentForm.setDirty(false); //need this???
		//sets controls to service on component init
		this.currentForm.setControls(this.experienceForm.controls);
		//subscribe for changes in form to be updated 
		this.experienceForm.statusChanges.subscribe(status=>{
			//updates form status on any change that happens
			this.currentForm.updateCurrent( this.experienceForm.status );
			//updates property to dirty whether form is valid or not
		});
	}
	
	//improved version from personalInformation...had to still use timeout so focusout does not override eraseContent
	//need to send a flag to differentiate from show&&hide...still better than toggling a class
	/*showHideEraseComp(isShow){
		console.log( this.values.eraseContent.value );
		if(isShow){
			console.log( this.values.eraseContent )
			this.values.eraseContent.value = false;
		}else{
			setTimeout(()=>{
				this.values.eraseContent.value = true;
			}, 100)
		}
	}*/

	//==========================TEMPORARY SOLUTION UNTIL DIRECTIVES ARE ADDED >.< ==============================
	showEraseComp(event){
		this.ARC.RemoveClass(event.srcElement.nextElementSibling.firstChild, "hide-erase-comp");
		this.ARC.AddClass(event.srcElement.nextElementSibling.firstChild, "show-erase-comp");
	}
	focusOut(event){
		let that = this;
		// wait 1 ms so event does not disappear
		setTimeout(function(){
			that.ARC.RemoveClass(event.srcElement.nextElementSibling.firstChild, "show-erase-comp");
			that.ARC.AddClass(event.srcElement.nextElementSibling.firstChild, "hide-erase-comp");	
		}, 100);
	}
	//==================================================================
	//erases content for aparticular field using form builder setValue method
	eraseContent(which){
		this.experienceForm.controls[which].setValue('');
	}

	UpdateCounter($event){
		($event.keyCode==8)?this.counter -= 1:this.counter = $event.srcElement.textLength+1;
	}
	//sets values "currently working here" to false and changes text also for toggle button
	toggle(event, type){
		if(type=="work"){
			this.values.isWorkingHere.value = !this.values.isWorkingHere.value;
			this.values.isWorkingHere.text = (event.target.checked)?"ON":"OFF";
		}else if(type=="residency"){
			this.values.isResidency.value = !this.values.isResidency.value;
			this.values.isResidency.text = (event.target.checked)?"ON":"OFF";
		}
	}

	checkYear(event){
		let toYearElem = document.getElementById('toTimePeriod');
		//takes -2 because of blank and 'Year' option
		this.startIndex = event.target.selectedIndex;
		
		if(this.startIndex!=0 && this.startIndex != 1){
			for(var i=2; i<this.startIndex; i++ )
				(<HTMLOptionElement>(<HTMLSelectElement>document.getElementById('toTimePeriod')).options[i]).disabled = true;	
		}else{
			
			for(var i=0; i< (<HTMLSelectElement>document.getElementById('toTimePeriod')).options.length; i++ )
				(<HTMLOptionElement>(<HTMLSelectElement>document.getElementById('toTimePeriod')).options[i]).disabled = false;	
		}		
		/* COME BACK TO THIS...NOT SURE WHAT THIS DOES EXACTLY */
		// console.log( this.experienceForm.controls['fromTimePeriod'].find('fromYear').valueChanges.debounceTime(400) );
	}

	checkMonth(event){
		let fromMonth = (<HTMLSelectElement>document.getElementById('fromMonth')).selectedIndex;
		
		let toMonth = event.target.selectedIndex;
		
		if(fromMonth>toMonth){
			(<HTMLOptionElement>(<HTMLSelectElement>document.getElementById('toTimePeriod')).options[this.startIndex]).disabled = true;
		}else if(fromMonth==toMonth) {
			(<HTMLOptionElement>(<HTMLSelectElement>document.getElementById('toTimePeriod')).options[this.startIndex]).disabled = false;
		}
	}

	saveAndContinue(){
		//check if form is valid and controls are defined
		if(this.currentForm.status == "VALID" && this.currentForm.getControls() ){
			// console.log("form is valid and controls have been defined");
			if( this.experienceForm.touched ){
				
				if(this.location.path().indexOf('Edit')>0){
					this.experience.updateExperience( this.currentForm.controls, this.location.path().split("/")[3] );
					this.router.navigate(['Experience']);	
				//will create experience
				}else if(this.location.path().indexOf('Create')>0){
					this.experience.createExperience( this.currentForm.controls );
					this.router.navigate(['Experience']);
				}
				
			}else{
				//form has not been changed and can navigate to 'Education'
				this.router.navigate(['Education']);
			}
		}else{
			// console.info("This form is invalid or controls have not been defined");
		}
	}
	
	ngOnDestroy(){
		console.log("EditExperience being DESTROYED!");
		//set source path so coming back to here implementation works correctly
		this.currentForm.setSource( this.location.path() );
	}

	//sets values if coming from edit or directly with an experience ID
	setFormValues(myobj){
		this.experienceForm.controls['title'].setValue(myobj.title||'');
		this.experienceForm.controls['organization'].setValue(myobj.organizationName||'');
		this.experienceForm.controls['location'].setValue(myobj.location||'');
		(<FormGroup>this.experienceForm.controls['fromTimePeriod']).controls['fromMonth'].setValue( myobj.fromMonth.toLowerCase() ||'Month' );
		(<FormGroup>this.experienceForm.controls['fromTimePeriod']).controls['fromYear'].setValue(myobj.fromYear||'Year');
		(<FormGroup>this.experienceForm.controls['toTimePeriod']).controls['toMonth'].setValue(myobj.toMonth.toLowerCase() ||'Month');
		(<FormGroup>this.experienceForm.controls['toTimePeriod']).controls['toYear'].setValue(myobj.toYear||'Year');
		this.experienceForm.controls['description'].setValue(myobj.description||'');
		this.experienceForm.controls['isResidency'].setValue(myobj.isResidency||false);
		this.setToggleText(myobj.isResidency, "residency");
		this.experienceForm.controls['isWorkingHere'].setValue(myobj.isCurrentWorkLocation||false);
		this.setToggleText(myobj.isCurrentWorkLocation, "workStatus");
	}

	setToggleText(isOn, which){
		if(which=="residency")			this.values.isResidency.text = (isOn)? "ON":"OFF";
		 else if(which=="workStatus") 	this.values.isWorkingHere.text = (isOn)? "ON":"OFF";
	}
}