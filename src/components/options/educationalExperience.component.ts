import {Component} from '@angular/core';
// import {SubMenu} from '../subMenu.component';
// import { ThisForm } from '../thisForm.component';
import { EditIcon } from '../body/editIcon.component';
import { GoBack } from '../body/goback.component';
import { ThisTitle } from '../body/thisTitle.component';
import { ThisSave } from '../body/thisSave.component';
import { ShowDivs } from '../../services/showDivs.service';
import{ TitleService } from '../../services/thisTitle.service';

@Component({
	templateUrl: '../../assets/templates/body/options/educationalExperience.template.html'
	//styleUrls: ['../../assets/styles/css/content.css']/*,*/
	// providers: [TitleService]
	// directives: [SubMenu, GoBack, ThisTitle, ThisSave, ThisForm]
}) export class EducationalExperience{
	content: string = "This is Educational Experience Content";
	title:string = "Educational-Experience";
	description: string = "This is a description that goes on and on about how great the person is";
	schoolName: string = "old component";
	degree: string = "BS";
	degreeIn: string = "KnowitAll";
	constructor(
		private show: ShowDivs
		,private titleService:TitleService
	){
		titleService.changeValue("Education");
	}

	ngOnInit(){
	}
	

}