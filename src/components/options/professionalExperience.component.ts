import { Component, ElementRef, EventEmitter, Output, Input } from '@angular/core';
import { Location } from '@angular/common';
import { EditIcon } from '../body/editIcon.component';
import { ExperienceService } from '../../services/httpServices/experience.service';
import { CurrentForm } from '../../services/currentForm.service';
import { Router } from '@angular/router';

@Component({
	selector:'professional-experience'
	,templateUrl: '../../assets/templates/body/options/professionalExperience.template.html'
	,providers: [ ExperienceService ]
	,styles:[
		`
			.centered-label{
				width:50%;
				margin: 0px auto;
			}
		`
	]

	}) export class ProfessionalExperience{
		//TO TEST ONLY
		@Output() toTest = {
			isData : true,
			label : "Data"
		}
		//========================

		show_submenu:boolean = true;
		experiences=[];
		@Output() showApplicable:boolean = true;
		whichView:boolean=false;
		
		@Output() viewObj = {
			buttonText : ""
			,action: 'create'
			,title: ""
			,organizationName: ""
			,Dates: {
				fromMonth: ""
				,toMonth: ""
				,fromYear: ""
				,toYear: ""
			}
			,description: ""
			,isCurrentWorkLocation: true
			,isResidency: true
		}

		@Output() setDisable = new EventEmitter<boolean>(false);

		@Output() disable:boolean = false;
		constructor( 
			private eref:ElementRef 
			,private experienceServ:ExperienceService
			,private currentForm:CurrentForm
			,private location:Location
			,private router:Router
		){
			//subscribe to propery to toggle active/diasble 'add another button'
			this.setDisable.subscribe( (isDisable)=>{
				this.disable = isDisable;
			});

			//subscribe to service to receive data on init()
			this.experienceServ.getExperiences().subscribe((data)=>{
				if( data.json().content.length ){
					// console.log("Data available.... fire CP-40");
					this.experiences = data.json().content;
					this.whichView = true;
				}else{
					// console.log("no data available...fire CP-32");
					this.whichView = false;
				}
				this.setView(this.whichView);
			});
		}

		//make API call to populate list
		ngOnInit(){
			this.currentForm.setDirty(false);
			//set path for current Form
			//set controles to null since Experience has no form and no data to be saved
			
			this.currentForm.setControls(null);

			if( this.experiences.length == 0 )
				this.showApplicable = false;
		}
		Applicable(){ 
			this.setDisable.next(!this.disable);
		}

		setView(whichView){
			//Data is available fire CP-40
			if(whichView){
				this.viewObj.buttonText = "Add Another";
				this.showApplicable = true;
			//data is null || em[ty]
			}else{
				this.viewObj.buttonText = "Add Experience";
			}
			this.viewObj.action = "create";
		}

		saveAndContinue(){
			this.router.navigate(['/Education']);
		}

		//===============================================================
		//=========TO TEST ONLY==========================================
		/*SetData(){
			// console.log(this.toTest.isData);
			//no Data
			if(this.toTest.isData){
				this.experiences = null;
				this.toTest.isData = false;
				this.viewObj.buttonText = "Add Professional Experience";
				this.showApplicable = false;
			//there is Data
			}else{
				this.experienceServ.getExperiences().subscribe((data)=>{
					this.experiences = data.json().content;
				});
				this.toTest.isData = true;
				this.viewObj.buttonText = "Add Another";
				this.showApplicable = true;
			}
		}*/

		//===============================================================

		ngOnDestroy(){
			console.log("Experiences is DESTROYED");
			console.log("Setting source path");
			this.currentForm.setSource( this.location.path() );
		}
}