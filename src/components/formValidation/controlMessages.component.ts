/*
TODO
	template should always be present but only toggle error checking
	this will avoid pushing down other fields when error is fired
*/
import { Component, Input } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { ValidationService } from '../../services/validationService.service';
import { FireValidation } from '../../services/fireValidation.service';
import { Subscription } from 'rxjs/Subscription';
import { CurrentForm } from '../../services/currentForm.service';

@Component({
	selector: 'control-messages',
	template: '<div [hidden]="(errorMessage == null)" >{{errorMessage}}</div> '
})export class ControlMessages{

	@Input() control: FormControl;

	constructor(
		private fire:FireValidation
		,private current:CurrentForm){}

	get errorMessage(){
		for(let field in this.control.errors){
			return ValidationService.getValidatorsErrorMessage( field );
		}
	}
}