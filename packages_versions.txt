==========================================
==========================================
||	current working version RC.5		||
==========================================
==========================================

Angular2-Starter-File@ C:\Repos\CareerPortal
+-- @angular/common@2.0.0-rc.5
+-- @angular/compiler@2.0.0-rc.5
+-- @angular/core@2.0.0-rc.5
+-- @angular/forms@0.3.0
+-- @angular/http@2.0.0-rc.5
+-- @angular/platform-browser@2.0.0-rc.5
+-- @angular/platform-browser-dynamic@2.0.0-rc.5
+-- @angular/router@3.0.0-beta.1
+-- @angular2-material/button@2.0.0-alpha.7-2 extraneous
+-- @angular2-material/card@2.0.0-alpha.7-2 extraneous
+-- @angular2-material/checkbox@2.0.0-alpha.7-2 extraneous
+-- @angular2-material/core@2.0.0-alpha.7-2 extraneous
+-- @angular2-material/input@2.0.0-alpha.7-2 extraneous
+-- @angular2-material/progress-circle@2.0.0-alpha.7-2 extraneous
+-- @angular2-material/toolbar@2.0.0-alpha.7-2 extraneous
+-- UNMET PEER DEPENDENCY bootstrap@~3.3.x
+-- browser-sync@2.14.0
| +-- browser-sync-client@2.4.2
| | +-- etag@1.7.0
| | `-- fresh@0.3.0
| +-- browser-sync-ui@0.6.0
| | +-- async-each-series@0.1.1
| | +-- connect-history-api-fallback@1.3.0
| | +-- stream-throttle@0.1.3
| | | +-- commander@2.9.0
| | | | `-- graceful-readlink@1.0.1
| | | `-- limiter@1.1.0
| | `-- weinre@2.0.0-pre-I0Z7U9OV
| |   +-- express@2.5.11
| |   | +-- connect@1.9.2
| |   | | `-- formidable@1.0.17
| |   | +-- mime@1.2.4
| |   | +-- mkdirp@0.3.0
| |   | `-- qs@0.4.2
| |   +-- nopt@3.0.6
| |   | `-- abbrev@1.0.9
| |   `-- underscore@1.7.0
| +-- bs-recipes@1.2.2
| +-- chokidar@1.5.1
| | +-- anymatch@1.3.0
| | | `-- arrify@1.0.1
| | +-- async-each@1.0.0
| | +-- UNMET OPTIONAL DEPENDENCY fsevents@^1.0.0
| | +-- glob-parent@2.0.0
| | +-- inherits@2.0.1
| | +-- is-binary-path@1.0.1
| | | `-- binary-extensions@1.5.0
| | +-- is-glob@2.0.1
| | +-- path-is-absolute@1.0.0
| | `-- readdirp@2.1.0
| |   `-- set-immediate-shim@1.0.1
| +-- connect@3.4.1
| | +-- debug@2.2.0
| | +-- finalhandler@0.4.1
| | | `-- unpipe@1.0.0
| | +-- parseurl@1.3.1
| | `-- utils-merge@1.0.0
| +-- dev-ip@1.0.1
| +-- easy-extender@2.3.2
| | `-- lodash@3.10.1
| +-- eazy-logger@3.0.2
| | `-- tfunk@3.0.2
| |   `-- object-path@0.9.2
| +-- emitter-steward@1.0.0
| +-- fs-extra@0.26.7
| | +-- jsonfile@2.3.1
| | +-- klaw@1.3.0
| | `-- rimraf@2.5.4
| +-- http-proxy@1.13.3
| | +-- eventemitter3@1.2.0
| | `-- requires-port@1.0.0
| +-- immutable@3.8.1
| +-- localtunnel@1.8.1
| | +-- openurl@1.1.0
| | +-- request@2.65.0
| | | +-- aws-sign2@0.6.0
| | | +-- bl@1.0.3
| | | | `-- readable-stream@2.0.6
| | | +-- caseless@0.11.0
| | | +-- combined-stream@1.0.5
| | | | `-- delayed-stream@1.0.0
| | | +-- forever-agent@0.6.1
| | | +-- form-data@1.0.0-rc4
| | | | `-- async@1.5.2
| | | +-- har-validator@2.0.6
| | | | +-- is-my-json-valid@2.13.1
| | | | | +-- generate-function@2.0.0
| | | | | +-- generate-object-property@1.2.0
| | | | | | `-- is-property@1.0.2
| | | | | `-- jsonpointer@2.0.0
| | | | `-- pinkie-promise@2.0.1
| | | |   `-- pinkie@2.0.4
| | | +-- hawk@3.1.3
| | | | +-- boom@2.10.1
| | | | +-- cryptiles@2.0.5
| | | | +-- hoek@2.16.3
| | | | `-- sntp@1.0.9
| | | +-- http-signature@0.11.0
| | | | +-- asn1@0.1.11
| | | | +-- assert-plus@0.1.5
| | | | `-- ctype@0.5.3
| | | +-- isstream@0.1.2
| | | +-- json-stringify-safe@5.0.1
| | | +-- node-uuid@1.4.7
| | | +-- oauth-sign@0.8.2
| | | +-- qs@5.2.1
| | | +-- stringstream@0.0.5
| | | +-- tough-cookie@2.2.2
| | | `-- tunnel-agent@0.4.3
| | `-- yargs@3.29.0
| |   +-- camelcase@1.2.1
| |   `-- window-size@0.1.4
| +-- micromatch@2.3.8
| | +-- arr-diff@2.0.0
| | | `-- arr-flatten@1.0.1
| | +-- array-unique@0.2.1
| | +-- braces@1.8.5
| | | +-- expand-range@1.8.2
| | | | `-- fill-range@2.2.3
| | | |   +-- is-number@2.1.0
| | | |   +-- randomatic@1.1.5
| | | |   `-- repeat-string@1.5.4
| | | +-- preserve@0.2.0
| | | `-- repeat-element@1.1.2
| | +-- expand-brackets@0.1.5
| | | `-- is-posix-bracket@0.1.1
| | +-- extglob@0.3.2
| | +-- filename-regex@2.0.0
| | +-- is-extglob@1.0.0
| | +-- kind-of@3.0.4
| | | `-- is-buffer@1.1.4
| | +-- normalize-path@2.0.1
| | +-- object.omit@2.0.0
| | | +-- for-own@0.1.4
| | | | `-- for-in@0.1.5
| | | `-- is-extendable@0.1.1
| | +-- parse-glob@3.0.4
| | | +-- glob-base@0.3.0
| | | `-- is-dotfile@1.0.2
| | `-- regex-cache@0.4.3
| |   +-- is-equal-shallow@0.1.3
| |   `-- is-primitive@2.0.0
| +-- opn@3.0.3
| | `-- object-assign@4.1.0
| +-- portscanner@1.0.0
| | `-- async@0.1.15
| +-- qs@6.2.0
| +-- resp-modifier@6.0.2
| | `-- minimatch@3.0.3
| |   `-- brace-expansion@1.1.6
| |     +-- balanced-match@0.4.2
| |     `-- concat-map@0.0.1
| +-- rx@4.1.0
| +-- serve-index@1.7.3
| | +-- accepts@1.2.13
| | | `-- negotiator@0.5.3
| | +-- batch@0.5.3
| | +-- escape-html@1.0.3
| | `-- http-errors@1.3.1
| |   `-- statuses@1.3.0
| +-- serve-static@1.10.3
| | `-- send@0.13.2
| |   +-- depd@1.1.0
| |   +-- destroy@1.0.4
| |   +-- mime@1.3.4
| |   +-- range-parser@1.0.3
| |   `-- statuses@1.2.1
| +-- server-destroy@1.0.1
| +-- socket.io@1.4.6
| | +-- engine.io@1.6.9
| | | +-- accepts@1.1.4
| | | | +-- mime-types@2.0.14
| | | | | `-- mime-db@1.12.0
| | | | `-- negotiator@0.4.9
| | | +-- base64id@0.1.0
| | | +-- engine.io-parser@1.2.4
| | | | +-- after@0.8.1
| | | | +-- arraybuffer.slice@0.0.6
| | | | +-- base64-arraybuffer@0.1.2
| | | | +-- blob@0.0.4
| | | | +-- has-binary@0.1.6
| | | | | `-- isarray@0.0.1
| | | | `-- utf8@2.1.0
| | | `-- ws@1.0.1
| | |   +-- options@0.0.6
| | |   `-- ultron@1.0.2
| | +-- has-binary@0.1.7
| | | `-- isarray@0.0.1
| | +-- socket.io-adapter@0.4.0
| | | `-- socket.io-parser@2.2.2
| | |   +-- debug@0.7.4
| | |   +-- isarray@0.0.1
| | |   `-- json3@3.2.6
| | +-- socket.io-client@1.4.6
| | | +-- backo2@1.0.2
| | | +-- component-bind@1.0.0
| | | +-- component-emitter@1.2.0
| | | +-- engine.io-client@1.6.9
| | | | +-- component-inherit@0.0.3
| | | | +-- has-cors@1.1.0
| | | | +-- parsejson@0.0.1
| | | | +-- parseqs@0.0.2
| | | | +-- xmlhttprequest-ssl@1.5.1
| | | | `-- yeast@0.1.2
| | | +-- indexof@0.0.1
| | | +-- object-component@0.0.3
| | | +-- parseuri@0.0.4
| | | | `-- better-assert@1.0.2
| | | |   `-- callsite@1.0.0
| | | `-- to-array@0.1.4
| | `-- socket.io-parser@2.2.6
| |   +-- benchmark@1.0.0
| |   +-- component-emitter@1.1.2
| |   +-- isarray@0.0.1
| |   `-- json3@3.3.2
| +-- ua-parser-js@0.7.10
| `-- yargs@4.7.1
|   +-- camelcase@3.0.0
|   +-- cliui@3.2.0
|   | `-- wrap-ansi@2.0.0
|   +-- decamelize@1.2.0
|   +-- lodash.assign@4.1.0
|   +-- os-locale@1.4.0
|   | `-- lcid@1.0.0
|   |   `-- invert-kv@1.0.0
|   +-- pkg-conf@1.1.3
|   | +-- find-up@1.1.2
|   | | `-- path-exists@2.1.0
|   | +-- load-json-file@1.1.0
|   | | `-- pify@2.3.0
|   | `-- symbol@0.2.3
|   +-- read-pkg-up@1.0.1
|   | `-- read-pkg@1.1.0
|   |   `-- path-type@1.1.0
|   +-- require-main-filename@1.0.1
|   +-- set-blocking@1.0.0
|   +-- string-width@1.0.1
|   | +-- code-point-at@1.0.0
|   | | `-- number-is-nan@1.0.0
|   | `-- is-fullwidth-code-point@1.0.0
|   +-- window-size@0.2.0
|   +-- y18n@3.2.1
|   `-- yargs-parser@2.4.1
|     `-- camelcase@3.0.0
+-- cors@2.7.1
| `-- vary@1.1.0
+-- es6-promise@3.1.2
+-- es6-shim@0.35.1
+-- gulp@3.9.1
| +-- archy@1.0.0
| +-- chalk@1.1.3
| | +-- ansi-styles@2.2.1
| | +-- escape-string-regexp@1.0.5
| | +-- has-ansi@2.0.0
| | | `-- ansi-regex@2.0.0
| | +-- strip-ansi@3.0.1
| | `-- supports-color@2.0.0
| +-- deprecated@0.0.1
| +-- gulp-util@3.0.7
| | +-- array-differ@1.0.0
| | +-- array-uniq@1.0.3
| | +-- beeper@1.1.0
| | +-- dateformat@1.0.12
| | +-- fancy-log@1.2.0
| | | `-- time-stamp@1.0.1
| | +-- gulplog@1.0.0
| | | `-- glogg@1.0.0
| | +-- has-gulplog@0.1.0
| | | `-- sparkles@1.0.0
| | +-- lodash._reescape@3.0.0
| | +-- lodash._reevaluate@3.0.0
| | +-- lodash._reinterpolate@3.0.0
| | +-- lodash.template@3.6.2
| | | +-- lodash._basecopy@3.0.1
| | | +-- lodash._basetostring@3.0.1
| | | +-- lodash._basevalues@3.0.0
| | | +-- lodash._isiterateecall@3.0.9
| | | +-- lodash.escape@3.2.0
| | | | `-- lodash._root@3.0.1
| | | +-- lodash.keys@3.1.2
| | | | +-- lodash._getnative@3.9.1
| | | | +-- lodash.isarguments@3.0.9
| | | | `-- lodash.isarray@3.0.4
| | | +-- lodash.restparam@3.6.1
| | | `-- lodash.templatesettings@3.1.1
| | +-- multipipe@0.1.2
| | | `-- duplexer2@0.0.2
| | |   `-- readable-stream@1.1.14
| | |     `-- isarray@0.0.1
| | +-- object-assign@3.0.0
| | +-- replace-ext@0.0.1
| | `-- vinyl@0.5.3
| +-- interpret@1.0.1
| +-- liftoff@2.3.0
| | +-- extend@3.0.0
| | +-- findup-sync@0.4.2
| | | +-- detect-file@0.1.0
| | | | `-- fs-exists-sync@0.1.0
| | | `-- resolve-dir@0.1.1
| | |   `-- global-modules@0.2.3
| | |     +-- global-prefix@0.1.4
| | |     `-- is-windows@0.2.0
| | +-- fined@1.0.1
| | | +-- expand-tilde@1.2.2
| | | +-- lodash.assignwith@4.1.0
| | | +-- lodash.isarray@4.0.0
| | | +-- lodash.isempty@4.3.1
| | | +-- lodash.pick@4.3.0
| | | `-- parse-filepath@1.0.1
| | |   +-- map-cache@0.2.2
| | |   `-- path-root@0.1.1
| | |     `-- path-root-regex@0.1.2
| | +-- flagged-respawn@0.3.2
| | +-- lodash.isplainobject@4.0.5
| | +-- lodash.isstring@4.0.1
| | +-- lodash.mapvalues@4.5.1
| | +-- rechoir@0.6.2
| | `-- resolve@1.1.7
| +-- minimist@1.2.0
| +-- orchestrator@0.3.7
| | +-- end-of-stream@0.1.5
| | | `-- once@1.3.3
| | |   `-- wrappy@1.0.2
| | +-- sequencify@0.0.7
| | `-- stream-consume@0.1.0
| +-- pretty-hrtime@1.0.2
| +-- semver@4.3.6
| +-- tildify@1.2.0
| | `-- os-homedir@1.0.1
| +-- v8flags@2.0.11
| | `-- user-home@1.1.1
| `-- vinyl-fs@0.3.14
|   +-- defaults@1.0.3
|   +-- glob-stream@3.1.18
|   | +-- glob@4.5.3
|   | +-- glob2base@0.0.12
|   | | `-- find-index@0.1.1
|   | +-- minimatch@2.0.10
|   | +-- ordered-read-streams@0.1.0
|   | +-- through2@0.6.5
|   | | `-- readable-stream@1.0.34
|   | |   `-- isarray@0.0.1
|   | `-- unique-stream@1.0.0
|   +-- glob-watcher@0.0.6
|   | `-- gaze@0.5.2
|   |   `-- globule@0.1.0
|   |     +-- glob@3.1.21
|   |     | +-- graceful-fs@1.2.3
|   |     | `-- inherits@1.0.2
|   |     +-- lodash@1.0.2
|   |     `-- minimatch@0.2.14
|   +-- graceful-fs@3.0.8
|   +-- mkdirp@0.5.1
|   | `-- minimist@0.0.8
|   +-- strip-bom@1.0.0
|   | `-- first-chunk-stream@1.0.0
|   +-- through2@0.6.5
|   | `-- readable-stream@1.0.34
|   |   `-- isarray@0.0.1
|   `-- vinyl@0.4.6
|     `-- clone@0.2.0
+-- gulp-connect@4.2.0
| +-- connect@2.30.2
| | +-- basic-auth-connect@1.0.0
| | +-- body-parser@1.13.3
| | | +-- depd@1.0.1
| | | +-- iconv-lite@0.4.11
| | | +-- qs@4.0.0
| | | `-- raw-body@2.1.7
| | |   +-- bytes@2.4.0
| | |   `-- iconv-lite@0.4.13
| | +-- bytes@2.1.0
| | +-- connect-timeout@1.6.2
| | +-- content-type@1.0.2
| | +-- cookie@0.1.3
| | +-- cookie-parser@1.3.5
| | +-- cookie-signature@1.0.6
| | +-- csurf@1.8.3
| | | `-- csrf@3.0.3
| | |   +-- base64-url@1.2.2
| | |   +-- rndm@1.2.0
| | |   +-- tsscmp@1.0.5
| | |   `-- uid-safe@2.1.1
| | |     `-- random-bytes@1.0.0
| | +-- depd@1.0.1
| | +-- errorhandler@1.4.3
| | | `-- accepts@1.3.3
| | |   `-- negotiator@0.6.1
| | +-- express-session@1.11.3
| | | +-- crc@3.3.0
| | | +-- depd@1.0.1
| | | `-- uid-safe@2.0.0
| | |   `-- base64-url@1.2.1
| | +-- finalhandler@0.4.0
| | | `-- escape-html@1.0.2
| | +-- method-override@2.3.6
| | | `-- methods@1.1.2
| | +-- multiparty@3.3.2
| | | +-- readable-stream@1.1.14
| | | | `-- isarray@0.0.1
| | | `-- stream-counter@0.2.0
| | |   `-- readable-stream@1.1.14
| | |     `-- isarray@0.0.1
| | +-- on-headers@1.0.1
| | +-- pause@0.1.0
| | +-- qs@4.0.0
| | +-- response-time@2.3.1
| | | `-- depd@1.0.1
| | +-- type-is@1.6.13
| | | `-- media-typer@0.3.0
| | `-- vhost@3.0.2
| +-- connect-livereload@0.5.4
| +-- event-stream@3.3.4
| | +-- duplexer@0.1.1
| | +-- from@0.1.3
| | +-- pause-stream@0.0.11
| | +-- split@0.3.3
| | `-- stream-combiner@0.0.4
| +-- http2@3.3.4
| `-- tiny-lr@0.2.1
|   +-- body-parser@1.14.2
|   | +-- bytes@2.2.0
|   | +-- iconv-lite@0.4.13
|   | `-- qs@5.2.0
|   +-- faye-websocket@0.10.0
|   | `-- websocket-driver@0.6.5
|   |   `-- websocket-extensions@0.1.1
|   +-- livereload-js@2.2.2
|   `-- qs@5.1.0
+-- gulp-sass@2.3.2
| +-- lodash.clonedeep@4.4.1
| +-- node-sass@3.8.0
| | +-- async-foreach@0.1.3
| | +-- cross-spawn@3.0.1
| | | +-- lru-cache@4.0.1
| | | | +-- pseudomap@1.0.2
| | | | `-- yallist@2.0.0
| | | `-- which@1.2.10
| | |   `-- isexe@1.1.2
| | +-- gaze@1.1.1
| | | `-- globule@1.0.0
| | |   `-- lodash@4.9.0
| | +-- get-stdin@4.0.1
| | +-- glob@7.0.5
| | | +-- fs.realpath@1.0.0
| | | `-- inflight@1.0.5
| | +-- in-publish@2.0.0
| | +-- meow@3.7.0
| | | +-- camelcase-keys@2.1.0
| | | | `-- camelcase@2.1.1
| | | +-- loud-rejection@1.6.0
| | | | +-- currently-unhandled@0.4.1
| | | | | `-- array-find-index@1.0.1
| | | | `-- signal-exit@3.0.0
| | | +-- map-obj@1.0.1
| | | +-- normalize-package-data@2.3.5
| | | | +-- hosted-git-info@2.1.5
| | | | +-- is-builtin-module@1.0.0
| | | | | `-- builtin-modules@1.1.1
| | | | `-- validate-npm-package-license@3.0.1
| | | |   +-- spdx-correct@1.0.2
| | | |   | `-- spdx-license-ids@1.2.2
| | | |   `-- spdx-expression-parse@1.0.2
| | | |     `-- spdx-exceptions@1.0.5
| | | +-- redent@1.0.0
| | | | +-- indent-string@2.1.0
| | | | `-- strip-indent@1.0.1
| | | `-- trim-newlines@1.0.0
| | +-- mkdirp@0.5.1
| | | `-- minimist@0.0.8
| | +-- nan@2.4.0
| | +-- node-gyp@3.4.0
| | | +-- fstream@1.0.10
| | | | `-- mkdirp@0.5.1
| | | |   `-- minimist@0.0.8
| | | +-- mkdirp@0.5.1
| | | | `-- minimist@0.0.8
| | | +-- npmlog@3.1.2
| | | | +-- are-we-there-yet@1.1.2
| | | | | `-- delegates@1.0.0
| | | | +-- console-control-strings@1.1.0
| | | | +-- gauge@2.6.0
| | | | | +-- aproba@1.0.4
| | | | | +-- has-unicode@2.0.1
| | | | | `-- wide-align@1.1.0
| | | | `-- set-blocking@2.0.0
| | | +-- osenv@0.1.3
| | | +-- path-array@1.0.1
| | | | `-- array-index@1.0.0
| | | |   `-- es6-symbol@3.1.0
| | | |     +-- d@0.1.1
| | | |     `-- es5-ext@0.10.12
| | | |       `-- es6-iterator@2.0.0
| | | +-- semver@5.3.0
| | | `-- tar@2.2.1
| | |   `-- block-stream@0.0.9
| | `-- sass-graph@2.1.2
| |   `-- lodash@4.15.0
| +-- through2@2.0.1
| | `-- readable-stream@2.0.6
| |   +-- core-util-is@1.0.2
| |   +-- isarray@1.0.0
| |   +-- process-nextick-args@1.0.7
| |   +-- string_decoder@0.10.31
| |   `-- util-deprecate@1.0.2
| `-- vinyl-sourcemaps-apply@0.2.1
+-- gulp-sourcemaps@1.6.0
| +-- convert-source-map@1.3.0
| +-- graceful-fs@4.1.5
| +-- strip-bom@2.0.0
| | `-- is-utf8@0.2.1
| `-- vinyl@1.2.0
|   +-- clone@1.0.2
|   `-- clone-stats@0.0.1
+-- gulp-tslint@3.6.0
| +-- map-stream@0.1.0
| +-- rcloader@0.1.4
| | `-- rcfinder@0.1.9
| +-- through@2.3.8
| `-- tslint@2.5.1
|   +-- findup-sync@0.2.1
|   | `-- glob@4.3.5
|   |   `-- minimatch@2.0.10
|   +-- optimist@0.6.1
|   | +-- minimist@0.0.10
|   | `-- wordwrap@0.0.3
|   `-- underscore.string@3.1.1
+-- gulp-typescript@2.13.6
| +-- source-map@0.5.6
| +-- typescript@1.8.10
| `-- vinyl-fs@2.4.3
|   +-- duplexify@3.4.5
|   | +-- end-of-stream@1.0.0
|   | `-- stream-shift@1.0.0
|   +-- glob-stream@5.3.2
|   | +-- glob@5.0.15
|   | +-- ordered-read-streams@0.3.0
|   | | `-- is-stream@1.1.0
|   | +-- through2@0.6.5
|   | | `-- readable-stream@1.0.34
|   | |   `-- isarray@0.0.1
|   | +-- to-absolute-glob@0.1.1
|   | | `-- extend-shallow@2.0.1
|   | `-- unique-stream@2.2.1
|   |   `-- json-stable-stringify@1.0.1
|   |     `-- jsonify@0.0.0
|   +-- is-valid-glob@0.3.0
|   +-- lazystream@1.0.0
|   +-- lodash.isequal@4.3.1
|   +-- merge-stream@1.0.0
|   +-- mkdirp@0.5.1
|   | `-- minimist@0.0.8
|   +-- readable-stream@2.1.4
|   | `-- buffer-shims@1.0.0
|   +-- strip-bom-stream@1.0.0
|   +-- through2-filter@2.0.0
|   +-- vali-date@1.0.0
|   `-- vinyl@1.2.0
+-- jasmine-core@2.4.1
+-- ng2-bs3-modal@0.7.0
+-- ng2-formly@2.0.0-beta.5
| +-- @angular/common@2.0.0-rc.1
| +-- @angular/compiler@2.0.0-rc.1
| +-- @angular/core@2.0.0-rc.1
| +-- @angular/platform-browser@2.0.0-rc.1
| +-- @angular/platform-browser-dynamic@2.0.0-rc.1
| +-- es6-shim@0.35.0
| +-- reflect-metadata@0.1.2
| `-- rxjs@5.0.0-beta.6
+-- reflect-metadata@0.1.3
+-- UNMET PEER DEPENDENCY rxjs@5.0.0-beta.11 invalid
| `-- symbol-observable@1.0.2
+-- superstatic@1.2.3
| +-- async@0.9.0
| +-- broker@1.0.1
| | +-- deliver@1.1.11
| | | +-- hyperquest@1.3.0
| | | | `-- through2@0.6.5
| | | |   `-- readable-stream@1.0.34
| | | |     `-- isarray@0.0.1
| | | `-- send@0.12.3
| | |   +-- depd@1.0.1
| | |   +-- destroy@1.0.3
| | |   +-- escape-html@1.0.1
| | |   +-- etag@1.6.0
| | |   | `-- crc@3.2.1
| | |   +-- fresh@0.2.4
| | |   +-- mime@1.3.4
| | |   `-- on-finished@2.2.1
| | |     `-- ee-first@1.1.0
| | `-- mime-types@1.0.2
| +-- cache-control@1.0.3
| | +-- cache-header@1.0.3
| | | `-- lodash.isstring@2.4.1
| | +-- glob-slasher@1.0.1
| | | `-- lodash.isobject@2.4.1
| | |   `-- lodash._objecttypes@2.4.1
| | +-- globject@1.0.1
| | | `-- minimatch@2.0.10
| | +-- lodash.isnumber@2.4.1
| | `-- regular@0.1.6
| +-- caseless@0.9.0
| +-- clean-urls@1.1.5
| | +-- as-array@1.0.0
| | | +-- lodash.isarguments@2.4.1
| | | `-- lodash.values@2.4.1
| | |   `-- lodash.keys@2.4.1
| | |     +-- lodash._isnative@2.4.1
| | |     `-- lodash._shimkeys@2.4.1
| | +-- booly@1.0.2
| | | `-- is-number@0.1.1
| | +-- join-path@1.1.0
| | | +-- as-array@2.0.0
| | | `-- url-join@0.0.1
| | +-- lodash.find@2.4.1
| | | +-- lodash.createcallback@2.4.4
| | | | +-- lodash._baseisequal@2.4.1
| | | | | +-- lodash._getarray@2.4.1
| | | | | | `-- lodash._arraypool@2.4.1
| | | | | +-- lodash._releasearray@2.4.1
| | | | | | `-- lodash._maxpoolsize@2.4.1
| | | | | +-- lodash.forin@2.4.1
| | | | | `-- lodash.isfunction@2.4.1
| | | | +-- lodash.keys@2.4.1
| | | | `-- lodash.property@2.4.1
| | | `-- lodash.forown@2.4.1
| | |   `-- lodash.keys@2.4.1
| | +-- mime-types@2.0.2
| | | `-- mime-db@1.1.2
| | `-- minimatch@1.0.0
| |   +-- lru-cache@2.7.3
| |   `-- sigmund@1.0.1
| +-- compression@1.5.2
| | +-- compressible@2.0.8
| | `-- vary@1.0.1
| +-- connect-query@0.2.0
| | `-- qs@1.1.0
| +-- fast-url-parser@1.1.3
| | `-- punycode@1.4.1
| +-- feedback@0.3.2
| | `-- chalk@0.3.0
| |   +-- ansi-styles@0.2.0
| |   `-- has-color@0.1.7
| +-- file-exists@0.1.1
| +-- lodash@2.4.2
| +-- mime-types@2.1.11
| | `-- mime-db@1.23.0
| +-- minimist@1.1.0
| +-- mix-into@0.4.0
| | `-- deap@0.2.2
| +-- morgan@1.6.1
| | +-- basic-auth@1.0.4
| | +-- depd@1.0.1
| | `-- on-finished@2.3.0
| |   `-- ee-first@1.1.1
| +-- nash@0.5.2
| | +-- args-list@0.3.3
| | +-- chalk@0.4.0
| | | +-- ansi-styles@1.0.0
| | | `-- strip-ansi@0.1.1
| | +-- drainer@0.2.1
| | | `-- as-array@0.1.2
| | |   `-- lodash.isarguments@2.4.1
| | +-- flat-arguments@0.3.0
| | | +-- as-array@0.1.2
| | | +-- flatten@0.0.1
| | | `-- lodash.isarguments@2.4.1
| | +-- lodash@2.4.2
| | +-- minimist@0.1.0
| | +-- pretty-print@0.4.1
| | | +-- chalk@0.3.0
| | | | `-- ansi-styles@0.2.0
| | | `-- lodash@2.2.1
| | +-- qmap@0.2.3
| | | +-- as-array@0.1.2
| | | `-- lodash.isarguments@2.4.1
| | `-- sorted-object@1.0.0
| +-- not-found@1.0.1
| | `-- is-url@1.2.2
| +-- pretty-print@1.0.0
| | +-- chalk@0.3.0
| | | `-- ansi-styles@0.2.0
| | `-- lodash@2.2.1
| +-- qs@2.3.3
| +-- redirects@1.1.1
| | +-- lodash.isobject@3.0.2
| | +-- pathematics@0.1.1
| | | +-- pathetic@0.3.1
| | | | +-- clone@0.1.19
| | | | +-- merge@1.1.3
| | | | `-- path-to-regexp@0.1.7
| | | `-- reverend@0.2.0
| | `-- toxic@1.0.0
| |   `-- lodash@2.4.2
| +-- serve-favicon@2.3.0
| | `-- ms@0.7.1
| +-- set-headers@1.0.0
| | `-- lodash.foreach@2.4.1
| |   `-- lodash._basecreatecallback@2.4.1
| |     +-- lodash._setbinddata@2.4.1
| |     | `-- lodash.noop@2.4.1
| |     +-- lodash.bind@2.4.1
| |     | +-- lodash._createwrapper@2.4.1
| |     | | +-- lodash._basebind@2.4.1
| |     | | | `-- lodash._basecreate@2.4.1
| |     | | `-- lodash._basecreatewrapper@2.4.1
| |     | `-- lodash._slice@2.4.1
| |     +-- lodash.identity@2.4.1
| |     `-- lodash.support@2.4.1
| +-- slasher@1.1.0
| +-- slashify@1.0.0
| +-- stacked@1.1.1
| +-- static-router@1.1.0
| | +-- directory-index@0.1.0
| | | `-- slasher@0.1.5
| | +-- glob-slash@1.0.0
| | `-- minimatch@1.0.0
| +-- update-notifier@0.2.2
| | +-- chalk@0.5.1
| | | +-- ansi-styles@1.1.0
| | | +-- has-ansi@0.1.0
| | | | `-- ansi-regex@0.2.1
| | | +-- strip-ansi@0.3.0
| | | `-- supports-color@0.2.0
| | +-- configstore@0.3.2
| | | +-- graceful-fs@3.0.8
| | | +-- js-yaml@3.6.1
| | | | +-- argparse@1.0.7
| | | | | `-- sprintf-js@1.0.3
| | | | `-- esprima@2.7.2
| | | +-- mkdirp@0.5.1
| | | | `-- minimist@0.0.8
| | | +-- object-assign@2.1.1
| | | +-- uuid@2.0.2
| | | `-- xdg-basedir@1.0.1
| | +-- is-npm@1.0.0
| | +-- latest-version@1.0.1
| | | `-- package-json@1.2.0
| | |   +-- got@3.3.1
| | |   | +-- infinity-agent@2.0.3
| | |   | +-- is-redirect@1.0.0
| | |   | +-- lowercase-keys@1.0.0
| | |   | +-- nested-error-stacks@1.0.2
| | |   | +-- object-assign@3.0.0
| | |   | +-- prepend-http@1.0.4
| | |   | +-- read-all-stream@3.1.0
| | |   | `-- timed-out@2.0.0
| | |   `-- registry-url@3.1.0
| | +-- semver-diff@2.1.0
| | `-- string-length@1.0.1
| `-- van@0.0.4
|   +-- chalk@0.4.0
|   | +-- ansi-styles@1.0.0
|   | `-- strip-ansi@0.1.1
|   `-- lodash@2.4.2
+-- symbol-observable@0.2.4
+-- systemjs@0.19.27
| `-- when@3.7.7
+-- typings@0.8.1
| +-- any-promise@1.3.0
| +-- bluebird@3.4.1
| +-- columnify@1.5.4
| | `-- wcwidth@1.0.1
| +-- listify@1.0.0
| +-- typings-core@0.3.1
| | +-- configstore@2.0.0
| | | +-- dot-prop@2.4.0
| | | | `-- is-obj@1.0.1
| | | +-- os-tmpdir@1.0.1
| | | +-- write-file-atomic@1.1.4
| | | | +-- imurmurhash@0.1.4
| | | | `-- slide@1.1.6
| | | `-- xdg-basedir@2.0.0
| | +-- detect-indent@4.0.0
| | | `-- repeating@2.0.1
| | |   `-- is-finite@1.0.1
| | +-- has@1.0.1
| | | `-- function-bind@1.1.0
| | +-- invariant@2.2.1
| | | `-- loose-envify@1.2.0
| | |   `-- js-tokens@1.0.3
| | +-- is-absolute@0.2.5
| | | +-- is-relative@0.2.1
| | | | `-- is-unc-path@0.1.1
| | | |   `-- unc-path-regex@0.1.2
| | | `-- is-windows@0.1.1
| | +-- lockfile@1.0.1
| | +-- make-error-cause@1.2.1
| | | `-- make-error@1.2.0
| | +-- mkdirp@0.5.1
| | | `-- minimist@0.0.8
| | +-- object.pick@1.1.2
| | | `-- isobject@2.1.0
| | +-- parse-json@2.2.0
| | | `-- error-ex@1.3.0
| | |   `-- is-arrayish@0.2.1
| | +-- popsicle@5.0.1
| | | +-- concat-stream@1.5.1
| | | | +-- readable-stream@2.0.6
| | | | `-- typedarray@0.0.6
| | | `-- form-data@0.2.0
| | |   +-- async@0.9.2
| | |   +-- combined-stream@0.0.7
| | |   | `-- delayed-stream@0.0.5
| | |   `-- mime-types@2.0.14
| | |     `-- mime-db@1.12.0
| | +-- popsicle-proxy-agent@1.0.0
| | | +-- http-proxy-agent@1.0.0
| | | | `-- agent-base@2.0.1
| | | |   `-- semver@5.0.3
| | | `-- https-proxy-agent@1.0.0
| | +-- popsicle-retry@2.0.0
| | +-- popsicle-status@1.0.2
| | +-- promise-finally@2.2.1
| | +-- rc@1.1.6
| | | +-- deep-extend@0.4.1
| | | +-- ini@1.3.4
| | | `-- strip-json-comments@1.0.4
| | +-- sort-keys@1.1.2
| | | `-- is-plain-obj@1.1.0
| | +-- string-template@1.0.0
| | +-- thenify@3.2.0
| | +-- throat@2.0.2
| | +-- touch@1.0.0
| | | `-- nopt@1.0.10
| | `-- zip-object@0.1.0
| +-- update-notifier@0.6.3
| | +-- boxen@0.3.1
| | | +-- filled-array@1.1.0
| | | `-- widest-line@1.0.0
| | +-- configstore@2.0.0
| | | +-- mkdirp@0.5.1
| | | | `-- minimist@0.0.8
| | | `-- xdg-basedir@2.0.0
| | `-- latest-version@2.0.0
| |   `-- package-json@2.3.3
| |     `-- got@5.6.0
| |       +-- create-error-class@3.0.2
| |       | `-- capture-stack-trace@1.0.0
| |       +-- duplexer2@0.1.4
| |       +-- is-retry-allowed@1.1.0
| |       +-- node-status-codes@1.0.0
| |       +-- unzip-response@1.0.0
| |       `-- url-parse-lax@1.0.0
| +-- wordwrap@1.0.0
| `-- xtend@4.0.1
`-- zone.js@0.6.12

